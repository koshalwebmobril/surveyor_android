package com.surveyor.Models;

public class UpComingJobsModel
{
    private int id;
    private String name,address,phone_no,lat,lang;
    private String image,job_id,new_form_id;

    /*public UpComingJobsModel()
    {

        this.image=image;
        this.name=name;
        this.address=address;
        this.phone_no=phone_no;
    }*/


    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name =name;
    }

    public String getImage()
    {
        return image;
    }
    public void setImage(String image)
    {
        this.image =image;
    }



    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address =address;
    }


    public String getPhoneno()
    {
        return phone_no;
    }
    public void setPhoneno(String phone_no)
    {
        this.phone_no =phone_no;
    }

    public String getLat()
    {
        return lat;
    }
    public void setLat(String lat)
    {
        this.lat =lat;
    }

    public String getLang()
    {
        return lang;
    }
    public void setLang(String lang)
    {
        this.lang =lang;
    }

    public String getJobId()
    {
        return job_id;
    }
    public void setJobId(String job_id)
    {
        this.job_id =job_id;
    }

    public String getNewFormId()
    {
        return new_form_id;
    }
    public void setNewFormId(String new_form_id)
    {
        this.new_form_id =new_form_id;
    }

}
