package com.surveyor.Models;

public class HistoryModel
{

    private int id;
    private String name,address,phone_no,time_taken,distance;
    private String image;

    /*public HistoryModel()
    {

        this.image=image;
        this.name=name;
        this.address=address;
        this.phone_no=phone_no;
    }*/


    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name =name;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image =image;
    }



    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address =address;
    }


    public String getTimeTaken()
    {
        return time_taken;
    }

    public void setTimeTaken(String time_taken)
    {
        this.time_taken =time_taken;
    }


    public String getPhoneno()
    {
        return phone_no;
    }

    public void setPhoneno(String phone_no)
    {
        this.phone_no =phone_no;
    }


    public String getDistance()
    {
        return distance;
    }
    public void setDistance(String distance)
    {
        this.distance =distance;
    }

}
