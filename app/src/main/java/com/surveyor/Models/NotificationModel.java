package com.surveyor.Models;

public class NotificationModel
{

    private int id;
    private String title,title_text,timing;
    private int image;

    public NotificationModel(int image, String title, String title_text,String timing)
    {

        this.image=image;
        this.title=title;
        this.title_text=title_text;
        this.timing=timing;
    }

    public NotificationModel() { }


    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title =title;
    }

    public int getImage()
    {
        return image;
    }
    public void setImage(int image)
    {
        this.image =image;
    }



    public String getTitleText()
    {
        return title_text;
    }
    public void setTitleText(String title_text)
    {
        this.title_text =title_text;
    }

    public String getTiming()
    {
        return timing;
    }
    public void setTiming(String timing)
    {
        this.timing =timing;
    }

}
