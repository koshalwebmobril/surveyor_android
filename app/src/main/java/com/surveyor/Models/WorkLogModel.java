package com.surveyor.Models;

public class WorkLogModel
{

    private int id;
    private String name,address,phone_no,date,description,time_taken,distance,submitted_date;
    private String image;

    public WorkLogModel() {

    }

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name =name;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date =date;
    }



    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image =image;
    }



    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address =address;
    }


    public String getPhoneno()
    {
        return phone_no;
    }
    public void setPhoneno(String phone_no)
    {
        this.phone_no =phone_no;
    }


    public String getDesciption()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description =description;
    }

    public String getTimeTaken()
    {
        return time_taken;
    }
    public void setTimeTaken(String time_taken)
    {
        this.time_taken =time_taken;
    }

    public String getDistance()
    {
        return distance;
    }
    public void setDistance(String distance)
    {
        this.distance =distance;
    }

    public String getSubmittedDate()
    {
        return submitted_date;
    }
    public void setSubmittedDate(String submitted_date)
    {
        this.submitted_date = submitted_date;
    }


}
