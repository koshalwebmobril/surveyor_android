package com.surveyor.Models;

import java.util.List;

public class ResponseModel {
    private boolean status;

    private List<Result> result;

    public void setStatus(boolean status){
        this.status = status;
    }
    public boolean getStatus(){
        return this.status;
    }
    public void setResult(List<Result> result){
        this.result = result;
    }
    public List<Result> getResult(){
        return this.result;
    }

    public class Result
    {
        private String date;


        private List<Data> data;

        public void setDate(String date){
            this.date = date;
        }
        public String getDate(){
            return this.date;
        }
        public void setData(List<Data> data){
            this.data = data;
        }
        public List<Data> getData(){
            return this.data;
        }
        public class Data
        {
            private int id;

            private String name;

            private int user_id;

            private String address;

            private String email;

            private String mobile;

            private String desc;

            private String time_taken;

            private String distance;

            private String live_photo;

            private String created_at;

            private String updated_at;

            private int status;

            public void setId(int id){
                this.id = id;
            }
            public int getId(){
                return this.id;
            }
            public void setName(String name){
                this.name = name;
            }
            public String getName(){
                return this.name;
            }
            public void setUser_id(int user_id){
                this.user_id = user_id;
            }
            public int getUser_id(){
                return this.user_id;
            }
            public void setAddress(String address){
                this.address = address;
            }
            public String getAddress(){
                return this.address;
            }
            public void setEmail(String email){
                this.email = email;
            }
            public String getEmail(){
                return this.email;
            }
            public void setMobile(String mobile){
                this.mobile = mobile;
            }
            public String getMobile(){
                return this.mobile;
            }
            public void setDesc(String desc){
                this.desc = desc;
            }
            public String getDesc(){
                return this.desc;
            }
            public void setTime_taken(String time_taken){
                this.time_taken = time_taken;
            }
            public String getTime_taken(){
                return this.time_taken;
            }
            public void setDistance(String distance){
                this.distance = distance;
            }
            public String getDistance(){
                return this.distance;
            }
            public void setLive_photo(String live_photo){
                this.live_photo = live_photo;
            }
            public String getLive_photo(){
                return this.live_photo;
            }
            public void setCreated_at(String created_at){
                this.created_at = created_at;
            }
            public String getCreated_at(){
                return this.created_at;
            }
            public void setUpdated_at(String updated_at){
                this.updated_at = updated_at;
            }
            public String getUpdated_at(){
                return this.updated_at;
            }
            public void setStatus(int status){
                this.status = status;
            }
            public int getStatus(){
                return this.status;
            }
        }
    }
}
