package com.surveyor.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.surveyor.Activities.BottomNavigationHome_Activity;
import com.surveyor.R;

import java.util.Map;
public class Notification_ServiceActivity extends FirebaseMessagingService
{
    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;

    public Notification_ServiceActivity()
    {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
              super.onMessageReceived(remoteMessage);
              Map<String, String> bundle = remoteMessage.getData();
              final String OrderId = remoteMessage.getData().get("title");
              final String message = remoteMessage.getData().get("message");
       //       final String orderid_withouttext= OrderId.substring(OrderId.indexOf("TM") , OrderId.length());

              Log.d("remoteMessage",  remoteMessage.toString());
              sendNotification(OrderId, message);
    }

    private void sendNotification(String OrderId, String message)
    {
        Intent intent = new Intent(getApplicationContext(), BottomNavigationHome_Activity.class);
      //  intent.putExtra("order_id",orderid_withouttext);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.logo)
                .setContentTitle(OrderId)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("MainChannel001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            notificationBuilder.setChannelId("MainChannel001");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        mNotificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}