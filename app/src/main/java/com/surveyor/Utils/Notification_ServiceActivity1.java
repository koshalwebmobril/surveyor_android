package com.surveyor.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.surveyor.Activities.BottomNavigationHome_Activity;
import com.surveyor.Models.NotificationModel;
import com.surveyor.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static android.app.Notification.DEFAULT_SOUND;
import static android.app.Notification.DEFAULT_VIBRATE;

public class Notification_ServiceActivity1 extends FirebaseMessagingService
{
    private static final String TAG = "MyFirebaseMsgService";
    NotificationCompat.Builder notificationBuilder;
    Bitmap image;
    Uri defaultSoundUri;

    private NotificationManager mNotificationManager;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private final static String default_notification_channel_id = "default";
    @Override
    public void onNewToken(String refreshedToken)
    {
        super.onNewToken(refreshedToken);
       // Singleton.getInstance().saveToken(this, refreshedToken);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        Log.i(TAG, "onMessageReceived: " + remoteMessage.getData());
        NotificationModel model = new NotificationModel();
        model.setTitle(remoteMessage.getData().get("title"));
        model.setId(Integer.parseInt(remoteMessage.getData().get("id")));
        model.setTitleText(remoteMessage.getData().get("message"));
        createNotification(model);


    }
    public void createNotification(NotificationModel model) {
        Intent notificationIntent = new Intent(this, BottomNavigationHome_Activity.class);
        int id = model.getId();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(model);
            out.flush();
            byte[] data = bos.toByteArray();
            notificationIntent.putExtra("INTENT_NOTIFICATION_DATA", data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultIntent = PendingIntent.getActivity(this, id, notificationIntent, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder
                (this, default_notification_channel_id);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(model.getTitleText());
        bigText.setBigContentTitle(model.getTitle());
        bigText.setSummaryText("Notification");
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
                .setSmallIcon(R.drawable.logo)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle(model.getTitle())
                .setContentText(model.getTitleText())
                .setContentIntent(resultIntent)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                .setStyle(bigText);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new
                    NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
        //FcmBroadcastReceiver.completeWakefulIntent(notificationIntent);
    }
}