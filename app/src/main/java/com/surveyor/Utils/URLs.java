package com.surveyor.Utils;

import android.annotation.SuppressLint;

public class URLs
{
    @SuppressLint("StaticFieldLeak")
    //  public static String BASE_URL = "http://52.54.1.108/surveyor/api/v1/auth/";

    public static String BASE_URL = "https://www.rdkinsurance.com/api/v1/auth/";
    public static String IMAGE_BASE_URL="https://www.rdkinsurance.com/";

    public static final String REGISTER=BASE_URL + "register";
    public static final String LOGIN = BASE_URL + "login";

    public static final String FORGOTPASSWORD = BASE_URL + "send_otp";
    public static final String VERIFYOTP = BASE_URL + "verify_otp";
    public static final String RESETPASSWORDAPI = BASE_URL + "reset_password";
    public static final String GETPROFILE = BASE_URL + "get_profile";
    public static final String UPDATEPROFILE = BASE_URL + "edit_profile";
    public static final String CHANGEPASSWORD = BASE_URL + "change_password";
    public static final String CONTACTUS = BASE_URL + "contact";
    public static final String PRIVACYPOLICY = BASE_URL + "privacy_policy";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String HISTORY = BASE_URL + "get_history";
    public static final String GETJOB = BASE_URL + "home";
    public static final String ADMINQUERY = BASE_URL + "admin_query";
    public static final String WORKLOGAPI = BASE_URL + "work_log";
    public static final String ADDNEWFORMAPI = BASE_URL + "add_new_form";
    public static final String NOTIFICATIONAPI = BASE_URL + "notifications";
    public static final String NAVIGATIONAPI = BASE_URL + "navigate";
    public static final String READNOTIFICATIONAPI = BASE_URL + "notificationsRead";
    public static final String NOTSUBMITAPI = BASE_URL + "not_add_form";
    public static final String NOTIFICATIONCOUNT = BASE_URL + "notificationsCount";
}