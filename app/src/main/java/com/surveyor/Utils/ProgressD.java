package com.surveyor.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.surveyor.R;


public class ProgressD extends Dialog {
    static ProgressD dialog;
    public ProgressD(Context context) {
        super(context);
    }

    private ProgressD(Context context, int theme) {
        super(context, theme);
    }

    public void setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            findViewById(R.id.message).setVisibility(View.VISIBLE);
            TextView txt = findViewById(R.id.message);
            txt.setText(message);
            txt.invalidate();
        }
    }

    public static ProgressD showLoading(Context context, CharSequence message) {
         dialog = new ProgressD(context, R.style.ProgressD);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("");
        dialog.setContentView(R.layout.layout_progress);
        if (message == null || message.length() == 0) {
            dialog.findViewById(R.id.message).setVisibility(View.GONE);
        } else {
            TextView txt = dialog.findViewById(R.id.message);
            txt.setText(message);
        }
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    public static  void hideProgressDialog() {
        try {
            if(dialog != null)
                dialog.dismiss();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
