package com.surveyor.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener;
import com.surveyor.R;
import com.surveyor.Adapters.RegisterAdapter;
import com.surveyor.Fragments.SignInFragment;
import com.surveyor.Fragments.SignUpFragment;
import com.surveyor.Interface.OnSignupClick;


import java.util.ArrayList;
public class MainActivity extends AppCompatActivity implements OnSignupClick
{
    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        simpleViewPager = (ViewPager) findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();

        fragmentArrayList.add(new SignInFragment());
        titleList.add("SIGN IN");

        fragmentArrayList.add(new SignUpFragment());
        titleList.add("SIGN UP");
        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(1);

        RegisterAdapter adapter = new RegisterAdapter(getSupportFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        // addOnPageChangeListener event change the tab on slide
        simpleViewPager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(tabLayout));

        simpleViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try
                {
                    SignInFragment.username.setText("");
                    SignInFragment.Password.setText("");

                    SignUpFragment.filename.setText("");
                    SignUpFragment.name.setText("");
                    SignUpFragment.Password.setText("");
                    SignUpFragment.Email.setText("");
                    SignUpFragment.Phone_number.setText("");
                }
                catch (Exception e){e.printStackTrace();}
            }

            @Override
            public void onPageSelected(int position) {
                try{
                    SignInFragment.username.setText("");
                    SignInFragment.Password.setText("");

                    final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(simpleViewPager.getWindowToken(), 0);
                }
                catch (Exception e){e.printStackTrace();}
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                try{
                    SignInFragment.username.setText("");
                    SignInFragment.Password.setText("");
                }catch (Exception e){e.printStackTrace();}
            }
        });

        try{
            String type=getIntent().getStringExtra("type");
            if (type.equals("signup")){
                simpleViewPager.setCurrentItem(1);
            }
        }catch (Exception e){e.printStackTrace();}
    }
    @Override
    public void onSignUp(boolean isSignUp)
    {
        if(isSignUp)
        {
            simpleViewPager.setCurrentItem(1);
        }
        else
        {
            simpleViewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onBackPressed()
    {
        finishAffinity();
        finish();
    }

}

