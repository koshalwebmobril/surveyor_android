package com.surveyor.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.surveyor.R;
import com.surveyor.Fragments.AccountFragment;
import com.surveyor.Fragments.HistoryFragment;
import com.surveyor.Fragments.UpComingJobFragment;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
public class BottomNavigationHome_Activity extends AppCompatActivity
{
    private FragmentTransaction ft;
    private Fragment currentFragment;
    BottomNavigationView bottomNav;
    Fragment homeFragment,historyFragment,accountFragment,navigationfragment;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_bottom_navigation_home_);
          bottomNav = findViewById(R.id.bottom_navigation);
          bottomNav.setOnNavigationItemSelectedListener(navListener);
         /* bottomNav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {

            }
        });*/
         if (savedInstanceState == null)
        {
           getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UpComingJobFragment(),"home").commit();
        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener()
            {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item)
                {
                    Fragment f=getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    switch (item.getItemId())
                    {
                        case R.id.nav_home:
                            if(!(f instanceof UpComingJobFragment)) {
                                ft = getSupportFragmentManager().beginTransaction();
                                currentFragment = new UpComingJobFragment();
                                ft.replace(R.id.fragment_container, currentFragment, "home");
                                //  ft.addToBackStack("home");
                                ft.commit();
                            }
                            break;

                        case R.id.nav_history:
                           /* if(!(f instanceof HistoryFragment))
                            {*/
                                ft = getSupportFragmentManager().beginTransaction();
                                currentFragment = new HistoryFragment();
                                ft.replace(R.id.fragment_container, currentFragment, "history").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                //  ft.addToBackStack("home");
                                ft.commit();
                                break;
                           // }

                        case R.id.nav_account:
                            if(!(f instanceof AccountFragment))
                            {
                                ft = getSupportFragmentManager().beginTransaction();
                                currentFragment = new AccountFragment();
                                ft.replace(R.id.fragment_container, currentFragment, "account").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                // ft.addToBackStack("home");
                                ft.commit();
                                break;
                            }
                    }
                    return true;
                }
            };
    @Override
    public void onBackPressed()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        homeFragment = fragmentManager.findFragmentByTag("home");
        historyFragment=fragmentManager.findFragmentByTag("history");
        accountFragment=fragmentManager.findFragmentByTag("account");
        if(historyFragment!=null && historyFragment.isVisible())
        {
            bottomNav.getMenu().getItem(0).setChecked(true);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UpComingJobFragment(),"home").commit();
        }

       else if(accountFragment!=null && accountFragment.isVisible())
        {
            bottomNav.getMenu().getItem(0).setChecked(true);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UpComingJobFragment(),"home").commit();
        }


        /*else if(navigationfragment!=null && navigationfragment.isVisible())
        {
            bottomNav.getMenu().getItem(0).setChecked(true);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new UpComingJobFragment(),"home").commit();
        }*/



       else if (homeFragment != null && homeFragment.isVisible())
        {
            new AlertDialog.Builder(this)
                       .setIcon(android.R.drawable.ic_dialog_alert)
                       .setTitle(getString(R.string.really_exit))
                       .setMessage(getString(R.string.are_sure_exit))
                       .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                       {
                           @Override
                           public void onClick(DialogInterface dialog, int which)
                           {
                               BottomNavigationHome_Activity.this.finishAffinity();
                           }
                       })
                       .setNegativeButton(getString(R.string.no), null).show();
           }
        else
           {
               fragmentManager.popBackStack();
           }
       }
}

