package com.surveyor.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.surveyor.R;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.FORGOTPASSWORD;
import static com.surveyor.Utils.URLs.VERIFYOTP;

public class VarificationCode_Activity extends AppCompatActivity implements View.OnClickListener
{
    Button btn_submit;
    private Window mWindow;
    EditText Email_otp1,Email_otp2,Email_otp3,Email_otp4,Email_otp5,Email_otp6;
    String code1,code2,code3,code4,code5,code6;
    String totalcode;
    String email;
    TextView resend_otp;
    private OtpTextView otpTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification_code_);
        init();

        otpTextView.setOtpListener(new OTPListener()
        {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }
            @Override
            public void onOTPComplete(String otp)
            {
                totalcode =otp.toString().trim();
                if(totalcode.length()<6)
                { }
                else
                {
                    hideKeyboard(otpTextView);
                }
            }
        });
    }

    public void init()
    {
        email = getIntent().getStringExtra("email");
        resend_otp=findViewById(R.id.resend_otp);
        otpTextView = findViewById(R.id.otp_view);

        btn_submit=findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        resend_otp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if (CommonMethod.isOnline(VarificationCode_Activity.this))
                {
                    totalcode=otpTextView.getOTP();
                    if(totalcode.length() < 6)
                   {
                       Toast.makeText(VarificationCode_Activity.this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
                       return;
                   }
                   else
                   {
                       hitVerifyOtpApi();
                   }
                   hideKeyboard((Button)v);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), VarificationCode_Activity.this);
                }
                break;
            case R.id.resend_otp:
                hitresendotpApi();
                break;
        }
    }
    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    public void hitVerifyOtpApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(this,"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("email",email);
            params.put("otp",totalcode);
            params.put("from_forgotpassword_screen","1");
            Log.d(TAG,"verify_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,VERIFYOTP, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.d(TAG,"forgotpassword_response" +response);
                    Toast.makeText(VarificationCode_Activity.this, message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        Intent i=new Intent(VarificationCode_Activity.this,ResetPassword_Activity.class);
                        i.putExtra("email",email);
                        startActivity(i);
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();

                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(VarificationCode_Activity.this, "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(this).addTorequestque(jsonObjReq);
    }

    public void hitresendotpApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(this,"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("mobile_or_email",email);
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,FORGOTPASSWORD, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");

                            Log.d(TAG,"forgotpassword_response" +response);

                            if (status.equals("true"))
                            {
                                Toast.makeText(VarificationCode_Activity.this, "OTP resent successfully to your registered Email", Toast.LENGTH_SHORT).show();
                                otpTextView.setOTP("");
                                progressDialog.dismiss();
                            }
                            else
                            {
                                Toast.makeText(VarificationCode_Activity.this, message, Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(VarificationCode_Activity.this, "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        }) {};
        Mysingleton.getInstance(VarificationCode_Activity.this).addTorequestque(jsonObjReq);
    }
}
