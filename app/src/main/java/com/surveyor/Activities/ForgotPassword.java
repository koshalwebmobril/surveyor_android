package com.surveyor.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.surveyor.R;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.CommonMethod.isValidEmaillId;
import static com.surveyor.Utils.URLs.FORGOTPASSWORD;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener
{
    Button btn_submit_password;
    EditText Email;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }
    public void init()
    {
        btn_submit_password=findViewById(R.id.btn_submit_password);
        Email=findViewById(R.id.Email);
        btn_submit_password.setOnClickListener(this);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit_password:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                        hitForgotPasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }
    private boolean  validation()
    {
        if(TextUtils.isEmpty(Email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isValidEmaillId(Email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    public void hitForgotPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(this,"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("mobile_or_email",Email.getText().toString().trim());
            Log.d(TAG,"forgot_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,FORGOTPASSWORD, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d(TAG,"login_response" +response);

                    if (status.equals("true"))
                    {
                        Intent i=new Intent(ForgotPassword.this,VarificationCode_Activity.class);
                        i.putExtra("email",Email.getText().toString().trim());
                        startActivity(i);
                        Toast.makeText(ForgotPassword.this, "OTP sent to your registered Email", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                    else
                    {
                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(ForgotPassword.this, "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(this).addTorequestque(jsonObjReq);
    }


}