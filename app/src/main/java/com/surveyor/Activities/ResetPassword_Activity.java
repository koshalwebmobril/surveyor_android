package com.surveyor.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.surveyor.R;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.CommonMethod.isValidEmaillId;
import static com.surveyor.Utils.URLs.RESETPASSWORDAPI;

public class ResetPassword_Activity extends AppCompatActivity implements View.OnClickListener
{
     EditText Email,Password,confirm_password;
     Button btn_submit;
     private Window mWindow;
     String email;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_);
        init();
    }

    public void init()
    {
        btn_submit=findViewById(R.id.btn_submit);
        Email=findViewById(R.id.Email);
        Password=findViewById(R.id.Password);
        confirm_password=findViewById(R.id.confirm_password);
        Email.setText(getIntent().getStringExtra("email"));
        btn_submit.setOnClickListener(this);
        Email.setFocusable(false);
        Email.setEnabled(false);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                        hitResetPasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }
    private boolean  validation()
    {
        if(TextUtils.isEmpty(Email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isValidEmaillId(Email.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(Password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(Password.getText().toString().trim().length()<8 || Password.getText().toString().trim().length()>16 )
        {
            Toast.makeText(this, "New Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(TextUtils.isEmpty(confirm_password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(confirm_password.getText().toString().trim().length()<8 || confirm_password.getText().toString().trim().length()>16 )
        {
            Toast.makeText(this, "Confirm Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        else if(!confirm_password.getText().toString().trim().equals(Password.getText().toString().trim()))
        {
            Toast.makeText(this, "Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hitResetPasswordApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(this,"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("new_password",Password.getText().toString().trim());
            params.put("email",Email.getText().toString().trim());
            params.put("confrim_new_password",confirm_password.getText().toString().trim());
            Log.d(TAG,"reset_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,RESETPASSWORDAPI, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.d(TAG,"reset_response" +response);
                    Toast.makeText(ResetPassword_Activity.this, message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        Intent i=new Intent(ResetPassword_Activity.this,MainActivity.class);
                        startActivity(i);
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(ResetPassword_Activity.this, "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(this).addTorequestque(jsonObjReq);
    }
    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }
}