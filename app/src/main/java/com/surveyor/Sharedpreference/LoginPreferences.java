package com.surveyor.Sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LoginPreferences
{
    private static com.surveyor.Sharedpreference.LoginPreferences preferences = null;
    private static SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String stateId;
    private Context context;

    private String user_id = "userid";
    private String track_id="trackid";
    private String user_status="userstatus";
    private String notificationcount="notificationcount";

    private String company_email="email";
    private String company_name="name";
    private String profile_image="profile_image";

    private String mobileno="mobileno";







    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    private void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static com.surveyor.Sharedpreference.LoginPreferences getActiveInstance(Context context)
    {
        if (preferences == null) {
            preferences = new com.surveyor.Sharedpreference.LoginPreferences(context);
        }
        return preferences;
    }

   /* public String getToken()
    {
        return mPreferences.getString(this.userid, "");
    }

    public void setToken(String token)
    {
        editor = mPreferences.edit();
        editor.putString(this.userid, token);
        editor.apply();
    }*/

    public void setUserName(String name)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_name, name);
        editor.apply();
    }

    public String getUserId()
    {
        return mPreferences.getString(this.user_id, "");
    }

    public void setUserId(String user_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.user_id, user_id);
        editor.apply();
    }


    public String getTrackId()
    {
        return mPreferences.getString(this.track_id, "null");
    }

    public void setTrackId(String track_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.track_id, track_id);
        editor.apply();
    }

    public String getUserName()
    {
        return mPreferences.getString(this.company_name, "");
    }

    public void setUserStatus(String user_status)
    {
        editor = mPreferences.edit();
        editor.putString(this.user_status, user_status);
        editor.apply();
    }

    public String getUserStatus()
    {
        return mPreferences.getString(this.user_status, "");
    }

    public static void deleteAllPreference()
    {
        mPreferences.edit().clear().apply();
    }

    public static void deleteoneitem(String selectedkey)
    {
        mPreferences.edit().remove(selectedkey).apply();
    }


    public String getUserProfile()
    {
        return mPreferences.getString(this.profile_image, "");
    }
    public void setUserProfile(String profile_image)
    {
        editor = mPreferences.edit();
        editor.putString(this.profile_image, profile_image);
        editor.apply();
    }


    /*public String getProviderId()
    {
        return mPreferences.getString(this.providerid, "");
    }


    public void setProviderId(String providerid)
    {
        editor = mPreferences.edit();
        editor.putString(this.providerid, providerid);
        editor.apply();
    }*/

    public String getnotificationcount()
    {
        return mPreferences.getString(this.notificationcount, "");
    }


    public void setNotificationcount(String notificationcount)
    {
        editor = mPreferences.edit();
        editor.putString(this.notificationcount, notificationcount);
        editor.apply();
    }

    public String getMobileno()
    {
        return mPreferences.getString(this.mobileno, "");
    }


    public void setMobileno(String mobileno)
    {
        editor = mPreferences.edit();
        editor.putString(this.mobileno, mobileno);
        editor.apply();
    }

/*

    public String getPageId()
    {
        return mPreferences.getString(this.page_id, "");
    }


    public void setPageId(String page_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.page_id, page_id);
        editor.apply();
    }
*/



}
