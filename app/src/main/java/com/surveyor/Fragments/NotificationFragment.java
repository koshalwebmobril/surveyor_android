package com.surveyor.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Adapters.NotificationAdapter;
import com.surveyor.Models.NotificationModel;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.ProgressD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.NOTIFICATIONAPI;
import static com.surveyor.Utils.URLs.READNOTIFICATIONAPI;

public class NotificationFragment extends Fragment implements View.OnClickListener
{

    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    EditText old_password,password,confirm_password;
    Button btn_submit;
    RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notificationmodellist;
    String user_id;
    String title,message,timing;
    LinearLayout linear_data,linear_nodata;
    TextView nodata_text;
    String readvalue="0";


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_notification, container, false);
        init();
        hitgetnotificationApi();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
         user_id = getArguments().getString("user_id");
         linear_data=view.findViewById(R.id.linear_data);
         linear_nodata=view.findViewById(R.id.linear_nodata);


         recyclerView =view.findViewById(R.id.recycle_item_list);
         notificationmodellist = new ArrayList<NotificationModel>();
         imgBack=view.findViewById(R.id.imgBack);
         btn_submit=view.findViewById(R.id.btn_submit);

        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        txtTitleOther.setText("NOTIFICATIONS");
        imgBack.setOnClickListener(this);



    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();                 //not parmanents
            //   backpressed();
               break;
        }
    }

    public void backpressed()
    {
        Fragment fragment = new UpComingJobFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"home");
            //ft.addToBackStack("");
            ft.commit();
        }
    }


    public void hitgetnotificationApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            Log.d(TAG,"notification_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,NOTIFICATIONAPI, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    Log.d(TAG,"notification_response" +response);

                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        JSONArray resultarray = jsonObject.getJSONArray("result");
                        for (int i = 0; i < resultarray.length(); i++)
                        {
                            NotificationModel notificationmodel = new NotificationModel();
                            JSONObject hit = resultarray.getJSONObject(i);
                            title = hit.getString("notification_title");
                            message= hit.getString("message");
                            timing=hit.getString("timing");

                            notificationmodel.setTitle(title);
                            notificationmodel.setTitleText(message);
                            notificationmodel.setTiming(timing);
                            notificationmodellist.add(notificationmodel);
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        notificationAdapter = new NotificationAdapter(getActivity(), (ArrayList) notificationmodellist);
                        recyclerView.setAdapter(notificationAdapter);

                        if(notificationAdapter.getItemCount()>0)
                        {
                            linear_nodata.setVisibility(View.GONE);
                            linear_data.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            linear_nodata.setVisibility(View.VISIBLE);
                            linear_data.setVisibility(View.GONE);
                        }
                        progressDialog.dismiss();


                        hitreadNotificationApi();                        //read notification api
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }

    public void hitreadNotificationApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id",LoginPreferences.getActiveInstance(getActivity()).getUserId());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,READNOTIFICATIONAPI, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    String status = response.getString("status");
                    if (status.equals("true"))
                    {
                        try {
                            LoginPreferences.getActiveInstance(getActivity()).setNotificationcount(readvalue);
                        }
                        catch (Exception e)
                        { }
                    }
                    else
                    { }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    // progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        })
        {};
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }
}