package com.surveyor.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.surveyor.R;

import static android.content.Context.MODE_PRIVATE;
import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;


public class WorkLogDetailsFragment extends Fragment implements View.OnClickListener
{
    View view;
    TextView name_text,date_text,address_text,phone_text,time,distance;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    Button btn_request_changes;
    String date,name_str,address_str,email,mobile,desc,time_taken,distance_traveled,created_at,image;
    ImageView user_image;
    TextView description_Text;
    String submitted_date;
    ProgressBar progress_bar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_work_log_details, container, false);
        init();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        final SharedPreferences login = getActivity().getSharedPreferences("Login_Token", MODE_PRIVATE);
        image = login.getString("image","");
        name_str = login.getString("name","");
        created_at = login.getString("submitted_date","");
        address_str = login.getString("address","");
        mobile = login.getString("phone_no","");
        time_taken = login.getString("time_taken","");
        desc=login.getString("description","");
        distance_traveled=login.getString("distance","");
        try
        {
            String[] splitStr = created_at.split("\\s+");
            submitted_date = splitStr[0];
        }
        catch (Exception e)
        { }

        progress_bar=view.findViewById(R.id.progress_bar);
        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        btn_request_changes= view.findViewById(R.id.btn_request_changes);

        txtTitleOther.setText("WORK LOG DETAILS");
        imgBack.setOnClickListener(this);
        btn_request_changes.setOnClickListener(this);
        name_text=view.findViewById(R.id.name_text);
        date_text=view.findViewById(R.id.date_text);
        address_text=view.findViewById(R.id.address_text);
        phone_text=view.findViewById(R.id.phone_text);
        time=view.findViewById(R.id.time);
        distance=view.findViewById(R.id.distance);
        description_Text=view.findViewById(R.id.description_Text);
        user_image=view.findViewById(R.id.user_image);

        name_text.setText(name_str);
        date_text.setText(submitted_date);
        address_text.setText("Address : " + address_str);
        phone_text.setText(mobile);
        time.setText(time_taken);
        distance.setText(distance_traveled);
        description_Text.setText(desc);

        if(image.equals("null"))
        {
           user_image.setImageResource(R.drawable.profile);
        }
        else
        {
         //   Picasso.get().load(IMAGE_BASE_URL+image).placeholder(getResources().getDrawable(R.drawable.user_profile_icon)).error(getResources().getDrawable(R.drawable.user_profile_icon)).into(user_image);
            Picasso.get()
                    .load(IMAGE_BASE_URL+image)
                    .into(user_image, new com.squareup.picasso.Callback()
                    {
                        @Override
                        public void onSuccess()
                        {
                            progress_bar.setVisibility(View.GONE);
                        }
                        @Override
                        public void onError(Exception e)
                        {
                            progress_bar.setVisibility(View.GONE);
                            user_image.setImageResource(R.drawable.user_profile_icon);
                        }
                    });
        }
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;
            case R.id.btn_request_changes:
               loadFragment(new RaiseRequestFragment());
                break;
        }
    }


    public void backpressed()
    {
        Fragment fragment = new WorkLogFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
           // ft.addToBackStack("");
            ft.commit();
        }
    }
    private void loadFragment(Fragment fragment)
    {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }
}