package com.surveyor.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;
import com.surveyor.Activities.MainActivity;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.GPSTracker;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.GETPROFILE;
import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;
import static com.surveyor.Utils.URLs.LOGOUT;
public class AccountFragment extends Fragment implements View.OnClickListener
{
    View view;
    private FragmentTransaction ft;
    AppCompatImageView main_bell;
    TextView txtTitleMain, edit_profile;
    LinearLayout linear_work_log, linear_change_password, linear_contact_us, linear_logout;
    String user_id, notificationcount;
    TextView name_text, email_text, phone_text;
    ImageView profile_imageView;
    String first_name, email, mobile, profile_image,username;
    LinearLayout linear_privacy_policy;
    ProgressBar progress_bar;
    TextView notificationcount_text;
    String user_status, tracking_id;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1 << 2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    private String user_lat,user_lng;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        init();
        user_id = LoginPreferences.getActiveInstance(getActivity()).getUserId();
        notificationcount =LoginPreferences.getActiveInstance(getActivity()).getnotificationcount();
        user_status =LoginPreferences.getActiveInstance(getActivity()).getUserStatus();
        tracking_id =LoginPreferences.getActiveInstance(getActivity()).getTrackId();

        if(notificationcount.equals("0") || notificationcount.equals(""))
        {
            notificationcount_text.setVisibility(View.GONE);
        }
        else
        {
            notificationcount_text.setVisibility(View.VISIBLE);
            notificationcount_text.setText(notificationcount);
        }
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void init() {

        progress_bar = view.findViewById(R.id.progress_bar);
        name_text = view.findViewById(R.id.name_text);
        email_text = view.findViewById(R.id.email_text);
        phone_text = view.findViewById(R.id.phone_text);
        profile_imageView = view.findViewById(R.id.profile_image);
        linear_logout = view.findViewById(R.id.linear_logout);
        linear_privacy_policy = view.findViewById(R.id.linear_privacy_policy);

        edit_profile = view.findViewById(R.id.edit_profile);
        linear_work_log = view.findViewById(R.id.linear_work_log);
        linear_change_password = view.findViewById(R.id.linear_change_password);
        linear_contact_us = view.findViewById(R.id.linear_contact_us);
        txtTitleMain = view.findViewById(R.id.txtTitleMain);
        notificationcount_text = view.findViewById(R.id.notificationcount_text);

        txtTitleMain.setText("MY PROFILE");
        main_bell = view.findViewById(R.id.main_bell);
        edit_profile.setOnClickListener(this);
        linear_work_log.setOnClickListener(this);
        linear_change_password.setOnClickListener(this);
        linear_contact_us.setOnClickListener(this);
        main_bell.setOnClickListener(this);
        linear_logout.setOnClickListener(this);
        linear_privacy_policy.setOnClickListener(this);


        hitGetProfileApi();
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_profile:
                EditProfileFragment ldf = new EditProfileFragment();
                Bundle args = new Bundle();
                args.putString("username", username);
                args.putString("phoneno", mobile);
                args.putString("user_id", user_id);
                args.putString("profile_image", profile_image);
                ldf.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, ldf).addToBackStack(null).commit();
                break;
            case R.id.linear_work_log:
                loadFragment(new WorkLogFragment());
                break;

            case R.id.linear_change_password:
                ChangePasswordFragment ldff = new ChangePasswordFragment();
                Bundle arg = new Bundle();
                arg.putString("user_id", user_id);
                ldff.setArguments(arg);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, ldff).addToBackStack(null).commit();
                break;

            case R.id.linear_privacy_policy:
                loadFragment(new PrivacyPolicyFragment());
                break;

            case R.id.linear_contact_us:
                ContactUsFragment con = new ContactUsFragment();
                Bundle ar = new Bundle();
                ar.putString("user_id", user_id);
                con.setArguments(ar);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, con).addToBackStack(null).commit();
                break;

            case R.id.main_bell:
                NotificationFragment notification = new NotificationFragment();
                Bundle arg1 = new Bundle();
                arg1.putString("user_id", user_id);
                notification.setArguments(arg1);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, notification).addToBackStack(null).commit();
                break;

            case R.id.linear_logout:
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.exit_application))
                        .setMessage(getString(R.string.are_sure_logout))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {

                               // LoginPreferences.deleteAllPreference();
                                if (CommonMethod.isOnline(getActivity()))
                                {
                                    checkPermission();
                                } else {
                                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                                }

                            }
                        })
                        .setNegativeButton(getString(R.string.no), null).show();
                break;
        }
    }

    public void hitGetProfileApi() {
        int MY_SOCKET_TIMEOUT_MS = 6000;

        final ProgressD progressDialog = ProgressD.showLoading(getActivity(), "please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
        }
        catch
        (Exception e) {
        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, GETPROFILE, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.d(TAG, "getprofile_response" + response);
                    //  Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true")) {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        JSONObject myResponse = jsonObject.getJSONObject("result");
                        first_name = myResponse.getString("first_name");
                        email = myResponse.getString("email");
                        mobile = myResponse.getString("mobile");
                        profile_image = myResponse.getString("profile_image");
                        username = myResponse.getString("username");

                        name_text.setText(username);
                        email_text.setText(email);
                        phone_text.setText(mobile);

                        if (profile_image.equals("null"))
                        {
                            profile_imageView.setImageResource(R.drawable.user_profile_icon);
                        }
                        else {
                            //  Picasso.get().load(IMAGE_BASE_URL+profile_image).placeholder(getResources().getDrawable(R.drawable.user_profile_icon)).error(getResources().getDrawable(R.drawable.user_profile_icon)).into(profile_imageView);
                            Picasso.get()
                                    .load(IMAGE_BASE_URL + profile_image)
                                    .into(profile_imageView, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            progress_bar.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError(Exception e) {
                                            progress_bar.setVisibility(View.GONE);
                                            profile_imageView.setImageResource(R.drawable.user_profile_icon);
                                        }
                                    });
                        }
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }

    public void hitLogutApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(), "please wait");
        JSONObject params = new JSONObject();
        try {
            params.put("userid", LoginPreferences.getActiveInstance(getActivity()).getUserId());
          //  params.put("user_lat", user_lat);
           // params.put("user_lng", user_lng);
            Log.d(TAG, "login_params" + params.toString());
        } catch
        (Exception e) {
        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, LOGOUT, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.d(TAG, "loGOUT_response" + response);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true")) {
                        LoginPreferences.deleteAllPreference();
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        }) {
        };
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }

    public void hitLogutApiActive()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(), "please wait");
        JSONObject params = new JSONObject();
        try {
            params.put("userid", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            params.put("user_status", LoginPreferences.getActiveInstance(getActivity()).getUserStatus());
            params.put("tracking_id", LoginPreferences.getActiveInstance(getActivity()).getTrackId());
            params.put("user_lat", user_lat);
            params.put("user_lng", user_lng);
            Log.d(TAG, "logout_params" + params.toString());
        } catch
        (Exception e) {
        }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST, LOGOUT, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.d(TAG, "loGOUT_response" + response);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        LoginPreferences.deleteAllPreference();
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        }) {
        };
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }
        if (granted) {
            checkGPS();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }

    private void toastNeedPermissions() {
        Toast.makeText(getActivity(), "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted) {
            } else {
                toastNeedPermissions();
            }
        }
    }

    private void checkGPS() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getActivity(), "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        } else
            {
            gpsTracker = new GPSTracker(getActivity());
            user_lat = String.valueOf(gpsTracker.getLatitude());
            user_lng = String.valueOf(gpsTracker.getLongitude());
              if(user_status.equals("active"))
                {
                    hitLogutApiActive();
                }
                else
                {
                    hitLogutApi();
                }
        }
    }
}
