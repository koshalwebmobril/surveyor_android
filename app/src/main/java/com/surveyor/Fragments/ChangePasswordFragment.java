package com.surveyor.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.MainActivity;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static com.surveyor.Utils.URLs.CHANGEPASSWORD;


public class ChangePasswordFragment extends Fragment implements View.OnClickListener
{
    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    EditText old_password,new_password,confirm_password;
    Button btn_submit;
    String user_id;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_change_password, container, false);
        init();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        imgBack=view.findViewById(R.id.imgBack);
        btn_submit=view.findViewById(R.id.btn_submit);
        old_password=view.findViewById(R.id.old_password);
        new_password=view.findViewById(R.id.new_password);
        confirm_password=view.findViewById(R.id.confirm_password);

        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        txtTitleOther.setText("CHANGE PASSWORD");
        imgBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

        user_id = getArguments().getString("user_id");
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;

            case R.id.btn_submit:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                       hitChangePasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;
        }
    }


    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
            //ft.addToBackStack("");
            ft.commit();
        }
    }
    private void loadFragment(Fragment fragment)
    {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit(); // save the changes
    }
    private boolean  validation()
    {
        if(TextUtils.isEmpty(old_password.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter Old Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(new_password.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter New password", Toast.LENGTH_SHORT).show();
            return false;
        }
         else if(new_password.getText().toString().trim().length()<8 || new_password.getText().toString().trim().length()>16 )
         {
             Toast.makeText(getActivity(), "New Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
             return false;
         }

         else if(TextUtils.isEmpty(confirm_password.getText().toString().trim()))
         {
             Toast.makeText(getActivity(), "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
             return false;
         }

         else if(confirm_password.getText().toString().trim().length()<8 || confirm_password.getText().toString().trim().length()>16 )
         {
             Toast.makeText(getActivity(), "Confirm Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
             return false;
         }

         else if(!confirm_password.getText().toString().trim().equals(new_password.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    public void hitChangePasswordApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            params.put("old_password",old_password.getText().toString().trim());
            params.put("password",new_password.getText().toString().trim());
            params.put("confirm_password",confirm_password.getText().toString().trim());

            Log.d(TAG,"change_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,CHANGEPASSWORD, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        SharedPreferences preferences =getActivity().getSharedPreferences("Login_Token", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        Intent i=new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        progressDialog.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }



}