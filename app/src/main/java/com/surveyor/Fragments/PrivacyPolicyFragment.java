package com.surveyor.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.surveyor.R;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import static com.surveyor.Utils.URLs.PRIVACYPOLICY;


public class PrivacyPolicyFragment extends Fragment implements View.OnClickListener
{
    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    TextView description_text;
    String description;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        init();
        hitPrivacyPolicyApi();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        description_text=view.findViewById(R.id.description_text);
        txtTitleOther.setText("PRIVACY POLICY");
        imgBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;
        }
    }


    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
            //ft.addToBackStack("");
            ft.commit();
        }
    }

    private  void hitPrivacyPolicyApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        StringRequest stringRequest = new StringRequest(Request.Method.GET,PRIVACYPOLICY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        try
                        {
                            JSONObject obj = new JSONObject(response);
                            if(obj.optString("status").equals("true"))
                            {
                                JSONObject myResponse1 = obj.getJSONObject("result");
                                description = myResponse1.getString("description");
                                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                {
                                        String textFromHtml = Jsoup.parse(description).text();
                                    description_text.setText(textFromHtml);
                                    //  description.setText(Html.fromHtml(page_description));
                                }
                                else
                                {
                                    String textFromHtml = Jsoup.parse(description).text();
                                    description_text.setText(textFromHtml);
                                    // description.setText(Html.fromHtml(page_description));
                                }
                                progressDialog.dismiss();
                            }
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        //displaying the error in toast if occurrs
                        //   Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}