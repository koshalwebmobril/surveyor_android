package com.surveyor.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Adapters.HistoryAdapter;
import com.surveyor.Models.HistoryModel;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.ProgressD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.HISTORY;

public class HistoryFragment extends Fragment implements View.OnClickListener
{
    View view;
    TextView txtTitleMain, upcoming_title;
    RecyclerView recyclerView;
    HistoryAdapter historyadapter;
    ArrayList<HistoryModel> historylist;
    private FragmentTransaction ft;
    Toolbar home_toolbar;
    AppCompatImageView main_bell;
    String user_id,notificationcount;
    String name,address,mobile,time_taken,distance,image;
    TextView appointment_history;
    LinearLayout linear_noitem,linear_items;
    TextView notificationcount_text;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_history, container, false);
        init();
        user_id = LoginPreferences.getActiveInstance(getActivity()).getUserId();
        notificationcount =LoginPreferences.getActiveInstance(getActivity()).getnotificationcount();

        if(notificationcount.equals("0") || notificationcount.equals(""))
        {
            notificationcount_text.setVisibility(View.GONE);
        }
        else
        {
            notificationcount_text.setVisibility(View.VISIBLE);
            notificationcount_text.setText(notificationcount);
        }
        hitHistoryApi();
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        main_bell=view.findViewById(R.id.main_bell);
        linear_noitem=view.findViewById(R.id.linear_noitem);
        linear_items=view.findViewById(R.id.linear_items);
        appointment_history=view.findViewById(R.id.appointment_history);
        txtTitleMain=view.findViewById(R.id.txtTitleMain);
        main_bell=view.findViewById(R.id.main_bell);
        txtTitleMain.setText("HISTORY");
        recyclerView =view.findViewById(R.id.recycle_item_list);
        notificationcount_text=view.findViewById(R.id.notificationcount_text);
        historylist = new ArrayList<HistoryModel>();

        main_bell.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
            {
                case R.id.main_bell:
                    NotificationFragment ldff = new NotificationFragment();
                    Bundle arg = new Bundle();
                    arg.putString("user_id", user_id);
                    ldff.setArguments(arg);
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, ldff).addToBackStack(null).commit();
                    break;
            }
    }


    public void hitHistoryApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            Log.d(TAG,"history_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,HISTORY, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    Log.d(TAG,"HISTORY_response" +response);

                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        JSONArray resultarray = jsonObject.getJSONArray("result");

                        for (int i = 0; i < resultarray.length(); i++)
                        {
                            HistoryModel historyModel = new HistoryModel();
                            JSONObject hit = resultarray.getJSONObject(i);
                            name = hit.getString("name");
                            address= hit.getString("address");
                            mobile = hit.getString("mobile");
                           // time_taken=hit.getString("time_taken");
                           // distance=hit.getString("distance");
                            image=hit.getString("live_photo");

                            historyModel.setName(name);
                            historyModel.setAddress(address);
                            historyModel.setPhoneno(mobile);
                            historyModel.setTimeTaken(time_taken);
                            historyModel.setDistance(distance);
                            historyModel.setImage(image);
                            historylist.add(historyModel);
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        historyadapter = new HistoryAdapter(getActivity(), (ArrayList) historylist);
                        recyclerView.setAdapter(historyadapter);
                        if(historyadapter.getItemCount()>0)
                        {
                            linear_noitem.setVisibility(View.GONE);
                            linear_items.setVisibility(View.VISIBLE);
                            appointment_history.setText("Appointments History");
                        }
                        else
                        {
                            linear_noitem.setVisibility(View.VISIBLE);
                            linear_items.setVisibility(View.GONE);
                        }
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }
}