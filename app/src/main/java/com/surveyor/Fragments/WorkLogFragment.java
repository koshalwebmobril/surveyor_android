package com.surveyor.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Adapters.WorkLogAdapter;
import com.surveyor.Models.ResponseModel;
import com.surveyor.Models.WorkLogModel;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.ProgressD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static com.surveyor.Utils.URLs.WORKLOGAPI;


public class WorkLogFragment extends Fragment implements View.OnClickListener
{

    View view;
    TextView txtTitleOther;
    RecyclerView recyclerView;
    WorkLogAdapter workLogAdapter;
    ArrayList<WorkLogModel> workLogModellist;

    List<ResponseModel.Result> list=new ArrayList<>();
    private FragmentTransaction ft;
    AppCompatImageView bell;
    AppCompatImageView imgBack;
    String user_id;
    String date,name,address,email,mobile,desc,time_taken,distance,live_photo,created_at;
    LinearLayout linear_nodata,linear_data;
    TextView date_textview;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_work_log, container, false);
        init();
        hitworklogApi();
       // getData();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        final SharedPreferences login = getActivity().getSharedPreferences("Login_Token", MODE_PRIVATE);
        user_id = login.getString("user_id", "");

     //   date_textview=view.findViewById(R.id.date_textview);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        linear_nodata=view.findViewById(R.id.linear_nodata);
        linear_data=view.findViewById(R.id.linear_data);

        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther.setText("WORK LOG");
        recyclerView =view.findViewById(R.id.recycle_item_list);
        workLogModellist = new ArrayList<WorkLogModel>();
        imgBack.setOnClickListener(this);
    }






    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;

        }
    }

    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
            ft.commit();
        }
    }

    public void hitworklogApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            Log.d(TAG,"worklog_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,WORKLOGAPI, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    Log.d(TAG,"worklog_job_response" +response);

                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        JSONArray resultarray = jsonObject.getJSONArray("result");
                        list.clear();
                        for (int i = 0; i < resultarray.length(); i++)
                        {
                            JSONObject hit = resultarray.getJSONObject(i);
                            JSONArray data = hit.getJSONArray("data");
                            if(data!=null&&data.length()>0)
                           {
                               Gson gson = new Gson();
                               ResponseModel resp = gson.fromJson(String.valueOf(response), ResponseModel.class);
                               list.add(resp.getResult().get(i));
                           }
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                                false));
                        workLogAdapter = new WorkLogAdapter(getActivity(), list);
                        recyclerView.setAdapter(workLogAdapter);


                      /*  if (resp.result.size() > 0 || resp.discoveryLikes.size() > 0) {
                            resultList.addAll(resp.result);
                            discoveryLikesList.addAll(resp.discoveryLikes);
                            discoveryAdapter.notifyDataSetChanged();
                        }*/

                      /*  for (int i = 0; i < resultarray.length(); i++)
                        {
                            JSONObject hit = resultarray.getJSONObject(i);
                            JSONArray data=hit.getJSONArray("data");
                            WorkLogModel worklogmodel = new WorkLogModel();
                            date=hit.getString("date");



                            *//*      if(!data.isNull(0))
                                {*//*
*//*
                                 for(int j = 0; j < data.length();j++)
                                    {
                                    JSONObject hit1 = data.getJSONObject(j);
                                    name = hit1.getString("name");
                                    address= hit1.getString("address");
                                    mobile = hit1.getString("mobile");
                                    desc =hit1.getString("desc");
                                    time_taken=hit1.getString("time_taken");
                                    distance=hit1.getString("distance");
                                    live_photo=hit1.getString("live_photo");
                                    created_at=hit1.getString("created_at");


                                }*//*
                                    worklogmodel.setDate(date);
                                  *//*  worklogmodel.setName(name);
                                    worklogmodel.setAddress(address);
                                    worklogmodel.setPhoneno(mobile);
                                    worklogmodel.setDescription(desc);
                                    worklogmodel.setDistance(distance);
                                    worklogmodel.setImage(live_photo);
                                    worklogmodel.setSubmittedDate(created_at);
                                    worklogmodel.setTimeTaken(time_taken);*//*

                                    workLogModellist.add(worklogmodel);
                          //  }
                        }*/
                        /*recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        workLogAdapter = new WorkLogAdapter(getActivity(), (ArrayList) workLogModellist);
                        recyclerView.setAdapter(workLogAdapter);*/


                        if(workLogAdapter.getItemCount()>0)
                        {
                            linear_nodata.setVisibility(View.GONE);
                            linear_data.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            linear_nodata.setVisibility(View.VISIBLE);
                            linear_data.setVisibility(View.GONE);
                        }
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }

}