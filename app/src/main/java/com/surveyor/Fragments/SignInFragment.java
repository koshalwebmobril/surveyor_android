package com.surveyor.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.surveyor.Activities.BottomNavigationHome_Activity;
import com.surveyor.Activities.ForgotPassword;
import com.surveyor.Activities.MainActivity;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Interface.OnSignupClick;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.LOGIN;
public class SignInFragment extends Fragment implements View.OnClickListener
{
    TextView signup_textview,forgot_password;
    Button btn_register;
    View view;
    public static EditText username,Password;
    ProgressDialog progressDialog;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String notification_token;
    private OnSignupClick onSignupClick;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
         view= inflater.inflate(R.layout.fragment_sign_in, container, false);
         init();
         checkLocationPermission();
         return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
        }
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if(context!=null)
            onSignupClick=(OnSignupClick) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    private void init()
    {
        signup_textview=view.findViewById(R.id.signup_textview);
        forgot_password= view.findViewById(R.id.forgot_password);
        btn_register=view.findViewById(R.id.btn_signin);
        username=view.findViewById(R.id.username);
        Password=view.findViewById(R.id.Password);

        signup_textview.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
        btn_register.setOnClickListener(this);

        notification_token = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG,notification_token);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_signin:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                   {
                      hitSignInApi();
                   }
            }
            else
            {
                CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
            }
            break;

            case R.id.signup_textview:
                Intent ii=new Intent(getActivity(), MainActivity.class);
                ii.putExtra("type","signup");
                startActivity(ii);
                requireActivity().overridePendingTransition(0,0);
                break;

            case R.id.forgot_password:
                Intent i=new Intent(getActivity(), ForgotPassword.class);
                startActivity(i);
                break;
        }
    }


    private boolean  validation()
    {
        if (TextUtils.isEmpty(username.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter username", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(Password.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hitSignInApi()
  {
      final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
       JSONObject params = new JSONObject();
    try
    {
            params.put("username",username.getText().toString().trim());
            params.put("password",Password.getText().toString().trim());
            params.put("device_type","1");
            params.put("device_token",notification_token);

            Log.d(TAG,"login_params"+ params.toString());
    }
    catch
    (Exception e) { }
    JsonObjectRequest jsonObjReq;
    jsonObjReq = new JsonObjectRequest(Request.Method.POST,LOGIN, params,new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try {
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.d(TAG,"login_response" +response);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                        if (status.equals("true"))
                        {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONObject myResponse = jsonObject.getJSONObject("result");
                          //  String token=myResponse.getString("token");

                            String user_id=myResponse.getString("id");
                            String user_name=myResponse.getString("username");

                            LoginPreferences.getActiveInstance(getActivity()).setUserId(user_id);
                            LoginPreferences.getActiveInstance(getActivity()).setUserName(user_name);
                            Intent i=new Intent(getActivity(), BottomNavigationHome_Activity.class);
                            startActivity(i);

                            username.setText("");
                            Password.setText("");
                            progressDialog.dismiss();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                          //  username.setText("");
                            Password.setText("");
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            Log.d(TAG, error.getMessage() + "error11");
            Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
            //  progressbar.setVisibility(GONE);
            progressDialog.dismiss();
            error.printStackTrace();
        }
    })
    {};
    Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
}

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        username.setText("");
        Password.setText("");
    }

    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }
}