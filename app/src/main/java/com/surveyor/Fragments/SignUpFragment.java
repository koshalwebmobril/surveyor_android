package com.surveyor.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.surveyor.Activities.MainActivity;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Interface.OnSignupClick;
import com.surveyor.R;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;
import com.surveyor.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.CommonMethod.isValidEmaillId;

import static com.surveyor.Utils.URLs.REGISTER;
public class SignUpFragment extends Fragment implements View.OnClickListener
{
    View view;
    TextView signin_textview;
    private OnSignupClick onSignupClick;
    private Button btn_register;
    public static EditText name, Email, Phone_number, Password;
    private ImageView uplaod_file;
    public static  TextView filename;
    private Uri filePath;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    byte[] mData1;
    private int clickImage;
    private int GALLERY = 1;
    private int REQUEST_CODE_DOC = 2;
    String extention;
    EditText ss_id;
    int len;
    int len1;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context != null)
            onSignupClick = (OnSignupClick) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        init();
        Phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (len > Phone_number.getText().length())
                {
                    len--;
                    return;
                }
                len = Phone_number.getText().length();

                if (len == 4 || len== 8) {
                    String number = Phone_number.getText().toString();
                    String dash = number.charAt(number.length() - 1) == '-' ? "" : "-";
                    number = number.substring(0, (len - 1)) + dash + number.substring((len - 1), number.length());
                    Phone_number.setText(number);
                    Phone_number.setSelection(number.length());
                }
            }
        });

        ss_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (len1 > ss_id.getText().length())
                {
                    len1--;
                    return;
                }
                len1 = ss_id.getText().length();

                if (len1 == 4 || len1== 8) {
                    String number = ss_id.getText().toString();
                    String dash = number.charAt(number.length() - 1) == '-' ? "" : "-";
                    number = number.substring(0, (len1 - 1)) + dash + number.substring((len1 - 1), number.length());
                    ss_id.setText(number);
                    ss_id.setSelection(number.length());
                }
            }
        });
        return view;
    }
    public void init()
    {
        signin_textview = view.findViewById(R.id.signin_textview);
        btn_register = view.findViewById(R.id.btn_signin);
        name = view.findViewById(R.id.name);
        Email = view.findViewById(R.id.Email);
        Phone_number = view.findViewById(R.id.Phone_number);
        Password = view.findViewById(R.id.Password);
        uplaod_file = view.findViewById(R.id.upload_file);
        filename=view.findViewById(R.id.filename);
        ss_id=view.findViewById(R.id.ss_id);

        signin_textview.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        uplaod_file.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if (validation())
                    {
                        hitSignUpApi();
                    }
                } else {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;
            case R.id.signin_textview:
              //  onSignupClick.onSignUp(false);
                Intent i=new Intent(getContext(),MainActivity.class);
                startActivity(i);
                break;

            case R.id.upload_file:
               // requestStoragePermission();
                showFileChooser();
                clickImage=1;
                filename.setText("");
                break;
        }
    }

    private boolean validation()
    {
        if (TextUtils.isEmpty(name.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(Email.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmaillId(Email.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (TextUtils.isEmpty(Phone_number.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().length()<7 || Phone_number.getText().toString().trim().length()>15 )
        {
            Toast.makeText(getActivity(), "Phone number must be 7 to 15 digit", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(Phone_number.getText().toString().trim().equals("0000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(Phone_number.getText().toString().trim().equals("00000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("0000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("00000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("0000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("00000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Phone_number.getText().toString().trim().equals("000000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(ss_id.getText().toString().trim().isEmpty())
        {
            Toast.makeText(getActivity(), "Please enter social security number(ssn)", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(ss_id.getText().toString().trim().length()<9)
        {
            Toast.makeText(getActivity(), "social security number must be 9 digit", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(Password.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(Password.getText().toString().trim().length()<8 || Password.getText().toString().trim().length()>16 )
        {
            Toast.makeText(getActivity(), "Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        else if (mData1 == null)
        {
            Toast.makeText(getActivity(), "Please Upload Document", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(filename.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Upload Document", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void showFileChooser()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
     //   String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","application/pdf","image/*"};
        String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(intent, REQUEST_CODE_DOC);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED)
        {
            return;
        }
        switch (clickImage)
        {
            case 1:
                if (requestCode == GALLERY)
                {
                    if (data != null)
                    {
                        Uri selectedImageUri = data.getData( );
                        Bitmap bitmap = null;
                        ByteArrayOutputStream bytes = null;
                        try
                        {
                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                            bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                            mData1 = bytes.toByteArray();

                            String picturePath = getPath(getActivity(), selectedImageUri);
                            String[] bits = picturePath.split("/");
                            String lastOne = bits[bits.length-1];
                            filename.setText(lastOne);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(requestCode==REQUEST_CODE_DOC && data!=null)
                {
                    Uri uri=data.getData();
                    Log.i(TAG, "onActivityResult: "+uri);
                    InputStream iStream = null;
                    try
                {
                    Cursor mCursor = getActivity().getApplicationContext().getContentResolver().query(uri, null, null, null, null);
                    int indexedname = mCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    mCursor.moveToFirst();
                    String filenamestring = mCursor.getString(indexedname);
                    mCursor.close();
                    iStream = getActivity().getContentResolver().openInputStream(uri);
                    extention=filenamestring.substring(filenamestring.lastIndexOf("."));
                    if(extention.equals(".png") || extention.equals(".jpg")  || extention.equals(".jpeg")
                            || extention.equals(".doc") || extention.equals(".pdf") || extention.equals(".docx"))
                    {
                        mData1 = getBytes(iStream);
                        filename.setText(filenamestring);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Invalid file format", Toast.LENGTH_SHORT).show();
                    }
                    Log.i(TAG, "onActivityResult11: "+extention);
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
             }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String getPath(Context context, Uri uri )
    {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

    public void hitSignUpApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        VolleyMultipartRequest volleyMultipartRequest = new  VolleyMultipartRequest(Request.Method.POST,REGISTER,new Response.Listener<NetworkResponse>()
        {
            @Override
            public void onResponse(NetworkResponse response)
            {
                try
                {
                    JSONObject object = new JSONObject(new String(response.data));
                    String status = object.getString("status");
                    String message = object.getString("message");

                    Log.d(TAG, "register_response" + response.data);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(new String(response.data));
                        JSONObject myResponse = jsonObject.getJSONObject("result");
                        String token=myResponse.getString("token");
                        String user_id=myResponse.getString("id");
                        Intent i=new Intent(getActivity(), MainActivity.class);
                        startActivity(i);

                        name.setText("");
                        Email.setText("");
                        Phone_number.setText("");
                        Password.setText("");
                        filename.setText("");
                        ss_id.setText("");
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();

                Log.d(TAG, "Failed with error msg:\t" + error.getMessage());
                Log.d(TAG, "Error StackTrace: \t" + error.getStackTrace());
                // edited here
                try {
                    byte[] htmlBodyBytes = error.networkResponse.data;
                    Log.e(TAG, new String(htmlBodyBytes), error);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("username", name.getText().toString().trim());
                params.put("email", Email.getText().toString().trim());
                params.put("mobile", Phone_number.getText().toString().trim());
                params.put("ss_id", ss_id.getText().toString().trim());
                params.put("password", Password.getText().toString().trim());
                params.put("device_type", "1");
                params.put("device_token", "jbfjkdf");
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData()
            {
                Map<String, DataPart> params = new HashMap<>();
                long imageName = System.currentTimeMillis();
                params.put("image", new DataPart(imageName + extention,mData1));
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Mysingleton.getInstance(getActivity()).addTorequestque(volleyMultipartRequest);
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(getActivity(), "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            }
            else
                {
                //Displaying another toast if permission is not granted
                Toast.makeText(getActivity(), "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SignInFragment.username.setText("");
        SignInFragment.Password.setText("");
     //   Toast.makeText(getActivity(), "onresume", Toast.LENGTH_SHORT).show();
    }
}
