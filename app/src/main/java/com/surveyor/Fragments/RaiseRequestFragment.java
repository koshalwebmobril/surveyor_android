package com.surveyor.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static com.surveyor.Utils.URLs.ADMINQUERY;


public class RaiseRequestFragment extends Fragment implements View.OnClickListener
{
    View view;
    EditText name,address,Email,phone_no;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    Button btn_submit;
    String user_id;
    EditText short_notes;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_raise_request, container, false);
        init();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {

        final SharedPreferences login = getActivity().getSharedPreferences("Login_Token", MODE_PRIVATE);
        user_id = login.getString("user_id", "");

        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        btn_submit=view.findViewById(R.id.btn_submit);
        short_notes=view.findViewById(R.id.short_notes);

        txtTitleOther.setText("RAISE A REQUEST TO ADMIN");
        imgBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;



            case R.id.btn_submit:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if (validation())
                    {
                        hitRaiseRequest();
                    }
                }
                else
                    {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;
        }
    }

    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
           // ft.addToBackStack("");
            ft.commit();
        }
    }

    public void hitRaiseRequest()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
            JSONObject params = new JSONObject();
            try
            {
                params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
                params.put("message",short_notes.getText().toString().trim());
                Log.d(TAG,"request_params"+ params.toString());
            }
            catch
            (Exception e) { }
            JsonObjectRequest jsonObjReq;
            jsonObjReq = new JsonObjectRequest(Request.Method.POST,ADMINQUERY, params,new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    try {
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                          if(status.equals("true"))
                          {
                              short_notes.setText("");
                              backpressed();
                              progressDialog.dismiss();
                          }
                          progressDialog.dismiss();
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.d(TAG, error.getMessage() + "error11");
                    Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                    //  progressbar.setVisibility(GONE);
                    progressDialog.dismiss();
                    error.printStackTrace();
                }
            })
            {};
            Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
        }

        public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }


    private boolean validation()
    {
        if (TextUtils.isEmpty(short_notes.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter note", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}