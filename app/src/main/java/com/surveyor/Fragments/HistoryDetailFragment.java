package com.surveyor.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.surveyor.R;

import org.apache.commons.lang3.StringUtils;

import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;
public class HistoryDetailFragment extends Fragment implements View.OnClickListener
{
    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    TextView name_text,address_text,phone_text,time_taken,distance_taken;
    ImageView user_image;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_history_detail, container, false);
        init();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);

        name_text=view.findViewById(R.id.name_text);
        address_text=view.findViewById(R.id.address);
        phone_text=view.findViewById(R.id.phone_text);
        time_taken=view.findViewById(R.id.time_taken);
        distance_taken=view.findViewById(R.id.distance_taken);
        user_image=view.findViewById(R.id.user_image);

        String name = getArguments().getString("name");
        String address = getArguments().getString("address");
        String phone = getArguments().getString("phone_no");
        String image = getArguments().getString("image");
        String distance = getArguments().getString("distance");
        String time = getArguments().getString("time");

        Glide.with(getActivity()).load(IMAGE_BASE_URL+image).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(user_image);

        txtTitleOther.setText("HISTORY DETAILS");
        name_text.setText(StringUtils.capitalize(name.toLowerCase().trim()));
        address_text.setText("Address : " + address);
        phone_text.setText(phone);
        time_taken.setText(time);
        distance_taken.setText(distance);
        imgBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgBack:
                backpressed();
                break;
        }
    }


    public void backpressed()
    {
        Fragment fragment = new HistoryFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"history");
           // ft.addToBackStack("");
            ft.commit();
        }
    }

}