package com.surveyor.Fragments;
import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;


import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.GPSTracker;
import com.surveyor.Utils.ProgressD;
import com.surveyor.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.ADDNEWFORMAPI;
import static com.surveyor.Utils.URLs.NOTSUBMITAPI;

public class AddNewFormFragment extends Fragment implements View.OnClickListener
{
    View view;
    AppCompatImageView imgBack;
    Button btn_submit, btn_not_submit;
    TextView txtTitleOther;
    String user_id;
    byte[] mData1;
    private int clickImage;
    private int GALLERY = 1, CAMERA = 2;
    private TextView filename;
    Bitmap bitmap = null;
    ImageView upload_file;
    private static final int PERM_CAMERA_REQUEST = 123;
    String new_form_id,User_name,phonenumber,user_address;
    String user_lat,user_lng;
    String lastOne;
    EditText username;
    String question1_carwash_string,question1_loans_string,question1_savings_money_string,question1_other_string,
            question2_fear_talking_topic_string,question2_leave_later_string,question2_think_too_expensive_string,
            question2_do_not_think_you_qualify_string,question3_yes_string,question3_no_string,question5_spouse_string,
            question5_child_string,question5_family_string,question5_friend_string,question5_other_string,question5_dont_know_string
            ,question6_car_wash_string,question6_loans_string,question6_savings_string,question6_other_string,question6_dont_know_string,
            language_english_String,langauge_spanish_string,question4_cremation_string,question4_burial_string;


    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView appointment_time, appointment_remainder_date, appointment_remainder_time,survey_time,appointment;

    EditText name, age,question4_usd,address, mobile,cell_phone,survey_day, notes, more_information;
    Calendar c5;


    CheckBox  question1_carwash, question1_loans, question1_savings_money, question1_other, question2_fear_talking_topic,
            question2_leave_later, question2_think_too_expensive, question2_do_not_think_you_qualify, question3_yes, question3_no,
            question4_burial, question4_cremation,
             question5_spouse, question5_child, question5_family, question5_friend, question5_other,question5_dont_know,
            question6_car_wash, question6_loans, question6_savings,
            question6_other, question6_dont_know,language_english, langauge_spanish;
         String username_string;
          String survey_day_String,survey_time_string;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    int len;

    RelativeLayout relative_yesnobutton;
    Button btn_yes,btn_no;
    LinearLayout linear_nextform;
    ScrollView scrollview;

     @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_add_new_form, container, false);
        init();

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);
        String currentdate = df.format(new Date());

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
        survey_day_String = formatter.format(cal.getTime());

        Date date = cal.getTime();
        int mHour1 = date.getHours();
        int mMinute1 = date.getMinutes();

        if(mMinute1<10)
        {
            survey_time.setText(mHour1 + ":" +"0"+ mMinute1);
        }
        else
        {
            survey_time.setText(mHour1 + ":" + mMinute1);
        }
        survey_day.setText(survey_day_String + " "+ currentdate);
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        user_id = LoginPreferences.getActiveInstance(getActivity()).getUserId();
        username_string = LoginPreferences.getActiveInstance(getActivity()).getUserName();

        scrollview=view.findViewById(R.id.scrollview);
        linear_nextform=view.findViewById(R.id.linear_nextform);
        relative_yesnobutton=view.findViewById(R.id.relative_yesnobutton);
        btn_yes=view.findViewById(R.id.btn_yes);
        btn_no=view.findViewById(R.id.btn_no);
        username=view.findViewById(R.id.username);
        appointment_time=view.findViewById(R.id.appointment_time);
        survey_time=view.findViewById(R.id.survey_time);
        name=view.findViewById(R.id.name);
        age =view.findViewById(R.id.age);
        question1_carwash =view.findViewById(R.id.question1_carwash);
        question1_loans =view.findViewById(R.id.question1_loans);
        question1_savings_money =view.findViewById(R.id.question1_savings_money);
        question1_other =view.findViewById(R.id.question1_other);
        question2_fear_talking_topic =view.findViewById(R.id.question2_fear_talking_topic);
        question2_leave_later =view.findViewById(R.id.question2_leave_later);
        question2_think_too_expensive =view.findViewById(R.id.question2_think_too_expensive);
        question2_do_not_think_you_qualify =view.findViewById(R.id.question2_do_not_think_you_qualify);
        question3_yes =view.findViewById(R.id.question3_yes);
        question3_no =view.findViewById(R.id.question3_no);
        question4_burial =view.findViewById(R.id.question4_burial);
        question4_cremation =view.findViewById(R.id.question4_cremation);
        question4_usd =view.findViewById(R.id.question4_usd);
        question5_spouse =view.findViewById(R.id.question5_spouse);
        question5_child =view.findViewById(R.id.question5_child);
        question5_family =view.findViewById(R.id.question5_family);
        question5_friend=view.findViewById(R.id.question5_friend);
        question5_other =view.findViewById(R.id.question5_other);
        question6_other =view.findViewById(R.id.question6_other);

        question6_car_wash =view.findViewById(R.id.question6_car_wash);
        question6_loans =view.findViewById(R.id.question6_loans);
        question6_savings =view.findViewById(R.id.question6_savings);
        question6_dont_know =view.findViewById(R.id.question6_dont_know);
        appointment =view.findViewById(R.id.appointment);

        address=view.findViewById(R.id.address);
        mobile =view.findViewById(R.id.mobile);
        cell_phone =view.findViewById(R.id.cell_phone);
        language_english =view.findViewById(R.id.language_english);
        langauge_spanish =view.findViewById(R.id.langauge_spanish);

        survey_day =view.findViewById(R.id.survey_day);
        survey_day.setEnabled(false);


        notes =view.findViewById(R.id.notes);
        appointment_remainder_date=view.findViewById(R.id.appointment_remainder_date);
        appointment_remainder_time=view.findViewById(R.id.appointment_remainder_time);
        more_information=view.findViewById(R.id.more_information);
        question5_dont_know =view.findViewById(R.id.question5_dont_know);

        upload_file=view.findViewById(R.id.upload_file);
        filename=view.findViewById(R.id.filename);
        imgBack=view.findViewById(R.id.imgBack);
        btn_submit=view.findViewById(R.id.btn_submit);
        btn_not_submit=view.findViewById(R.id.btn_not_submit);

        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        txtTitleOther.setText("SURVEY FORM");
        imgBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        upload_file.setOnClickListener(this);
        btn_yes.setOnClickListener(this);
        btn_no.setOnClickListener(this);
        appointment_time.setOnClickListener(this);
        appointment.setOnClickListener(this);

        // appointment_remainder_date.setOnClickListener(this);
       // appointment_remainder_time.setOnClickListener(this);

        username.setText(username_string);                //set name
        Log.d(TAG,username_string);
        username.setEnabled(false);

        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (len > mobile.getText().length())
                {
                    len--;
                    return;
                }
                len = mobile.getText().length();
                if (len == 4 || len== 8) {
                    String number = mobile.getText().toString();
                    String dash = number.charAt(number.length() - 1) == '-' ? "" : "-";
                    number = number.substring(0, (len - 1)) + dash + number.substring((len - 1), number.length());
                    mobile.setText(number);
                    mobile.setSelection(number.length());
                }
            }
        });
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    checkPermission();
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;


            case R.id.btn_yes:
                relative_yesnobutton.setVisibility(View.GONE);
                linear_nextform.setVisibility(View.VISIBLE);
                break;


            case R.id.btn_no:
                name.setText("");
                age.setText("");
                question1_carwash.setChecked(false);
                question1_loans.setChecked(false);
                question1_savings_money.setChecked(false);
                question1_other.setChecked(false);

                question2_fear_talking_topic.setChecked(false);
                question2_leave_later.setChecked(false);
                question2_think_too_expensive.setChecked(false);
                question2_do_not_think_you_qualify.setChecked(false);

                question3_yes.setChecked(false);
                question3_no.setChecked(false);
                question4_burial.setChecked(false);
                question4_cremation.setChecked(false);
                question4_usd.setText("");                  //edittext

                question5_spouse.setChecked(false);
                question5_child.setChecked(false);
                question5_family.setChecked(false);
                question5_friend.setChecked(false);
                question5_other.setChecked(false);
                question5_dont_know.setChecked(false);
                question6_car_wash.setChecked(false);
                question6_loans.setChecked(false);
                question6_savings.setChecked(false);
                question6_other.setChecked(false);
                question6_dont_know.setChecked(false);

                scrollview.fullScroll(ScrollView.FOCUS_UP);
                break;

            case R.id.imgBack:
               backpressed();
               break;

            case R.id.upload_file:
                checkRunTimePermission(v);
                break;


            case R.id.appointment:
                c5 = Calendar.getInstance();
                mYear = c5.get(Calendar.YEAR);
                mMonth = c5.get(Calendar.MONTH);
                mDay = c5.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog2 = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener()
                        {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth)
                            {
                                appointment.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog2.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog2.show();
                break;



            case R.id.appointment_time:
                Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener()
                        {
                         @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                         {
                             Calendar datetime = Calendar.getInstance();
                             datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                             datetime.set(Calendar.MINUTE, minute);
                             if(appointment.getText().toString().isEmpty())
                             {
                                 Toast.makeText(getActivity(), "Please Select Cita First", Toast.LENGTH_SHORT).show();
                             }
                             else
                             {
                                 String myDate=appointment.getText().toString()+" "+hourOfDay +":"+ minute;
                                 SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyy HH:mm");
                                 Date cD=Calendar.getInstance().getTime();
                                 Date myDat,currentDate;
                                 try {
                                      myDat=format.parse(myDate);
                                     String formattedDate = format.format(cD);
                                      currentDate=format.parse(formattedDate);
                                      if (myDat.before(currentDate))
                                      {
                                          Toast.makeText(requireContext(), "Please enter the correct time", Toast.LENGTH_SHORT).show();
                                          appointment_time.setText("");
                                      }
                                      else
                                          {
                                              if(minute<10)
                                              {
                                                  appointment_time.setText(hourOfDay + ":" +"0"+ minute);
                                              }
                                              else
                                              {
                                                  appointment_time.setText(hourOfDay + ":" + minute);
                                              }
                                      }
                                 } catch (ParseException e) {
                                     e.printStackTrace();
                                 }
                             }
                         }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;



            case R.id.appointment_remainder_date:
                final Calendar c2 = Calendar.getInstance();
                mYear = c2.get(Calendar.YEAR);
                mMonth = c2.get(Calendar.MONTH);
                mDay = c2.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                appointment_remainder_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;

            case R.id.appointment_remainder_time:
                final Calendar c3 = Calendar.getInstance();
                mHour = c3.get(Calendar.HOUR_OF_DAY);
                mMinute = c3.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener()
                        {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                            {
                                Calendar datetime = Calendar.getInstance();
                                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                datetime.set(Calendar.MINUTE, minute);
                                if(appointment_remainder_date.getText().toString().isEmpty())
                                {
                                    Toast.makeText(getActivity(), "Please Select Cita First", Toast.LENGTH_SHORT).show();

                                }
                                else
                                {
                                    String myDate=appointment_remainder_date.getText().toString()+" "+hourOfDay +":"+ minute;
                                    SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyy HH:mm");
                                    Date cD=Calendar.getInstance().getTime();
                                    Date myDat,currentDate;
                                    try
                                    {
                                        myDat=format.parse(myDate);
                                        String formattedDate = format.format(cD);
                                        currentDate=format.parse(formattedDate);
                                        if (myDat.before(currentDate))
                                        {
                                            Toast.makeText(requireContext(), "Please enter the correct time", Toast.LENGTH_SHORT).show();
                                            appointment_remainder_time.setText("");
                                        }
                                        else
                                        {
                                            if(minute<10)
                                            {
                                                appointment_remainder_time.setText(hourOfDay + ":" +"0"+ minute);
                                            }
                                            else
                                            {
                                                appointment_remainder_time.setText(hourOfDay + ":" + minute);
                                            }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog1.show();
                break;

                /*case R.id.survey_time:
                final Calendar c4 = Calendar.getInstance();
                mHour = c4.get(Calendar.HOUR_OF_DAY);
                mMinute = c4.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog4 = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if(minute<10)
                                {
                                    survey_time.setText(hourOfDay + ":" +"0"+ minute);
                                }
                                else
                                {
                                    survey_time.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog4.show();
                break;*/


              }
             }
    private boolean validation()
    {
        if(TextUtils.isEmpty(name.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter Nombre", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(appointment.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter Cita", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(appointment_time.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter Hora ", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(address.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter Dirección", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(TextUtils.isEmpty(mobile.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter Teléfono", Toast.LENGTH_SHORT).show();
            return false;
        }


        else if(TextUtils.isEmpty(more_information.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please Enter More Information", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(mData1 == null)
        {
            Toast.makeText(getActivity(), "Please Upload Live Photo", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void backpressed()
    {
        UpComingJobFragment ldff = new UpComingJobFragment();
        Bundle arg1 = new Bundle();
        ldff.setArguments(arg1);
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,ldff,"home").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

    public void hitAddNewFormApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        VolleyMultipartRequest volleyMultipartRequest = new  VolleyMultipartRequest(Request.Method.POST,ADDNEWFORMAPI,new Response.Listener<NetworkResponse>()
        {
            @Override
            public void onResponse(NetworkResponse response)
            {
                try
                {
                    JSONObject object = new JSONObject(new String(response.data));
                    String status = object.getString("status");
                    String message = object.getString("message");
                    Log.d(TAG, "addnew_response" + response.data);

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(new String(response.data));
                        JSONObject myResponse = jsonObject.getJSONObject("result");
                        Fragment fragment = new UpComingJobFragment();
                        FragmentTransaction ft = ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, fragment, "home").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String,String> params = new HashMap<>();
                params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());      //
                params.put("name", name.getText().toString().trim()); //
                params.put("address", address.getText().toString().trim());//
                params.put("desc", more_information.getText().toString().trim());//
                params.put("mobile", mobile.getText().toString().trim());//
                params.put("age", age.getText().toString().trim());//

                params.put("langauge_spanish", langauge_spanish_string);//
                params.put("language_english", language_english_String);//

                params.put("survey_day", survey_day.getText().toString().trim());//
                params.put("survey_time", survey_time.getText().toString().trim()); //

                params.put("notes", notes.getText().toString().trim());//
                params.put("cell_phone", cell_phone.getText().toString().trim());//
                params.put("appointment_time", appointment_time.getText().toString().trim());//
                params.put("appointment", appointment.getText().toString().trim());//

                params.put("question1_carwash", question1_carwash_string);//
                params.put("question1_loans", question1_loans_string);//
                params.put("question1_savings_money", question1_savings_money_string);//
                params.put("question1_other", question1_other_string);//
                params.put("question2_fear_talking_topic",  question2_fear_talking_topic_string);//
                params.put("question2_think_too_expensive", question2_think_too_expensive_string);//
                params.put("question2_leave_later",  question2_leave_later_string);//
                params.put("question2_do_not_think_you_qualify", question2_do_not_think_you_qualify_string);//
                params.put("question3_yes",  question3_yes_string);//
                params.put("question3_no",question3_no_string);//
                params.put("question4_cremation", question4_cremation_string);//
                params.put("question4_burial", question4_burial_string);//

                params.put("question4_usd", question4_usd.getText().toString().trim());//

                params.put("question5_spouse", question5_spouse_string);//
                params.put("question5_child", question5_child_string);//
                params.put("question5_family",  question5_family_string);//
                params.put("question5_friend",  question5_friend_string);//
                params.put("question6_savings", question6_savings_string);//
                params.put("question6_loans", question6_loans_string);//
                params.put("question5_other",question5_other_string);//
                params.put("question5_dont_know", question5_dont_know_string);//
                params.put("question6_car_wash", question6_car_wash_string);//
                params.put("question6_other",question6_other_string);//
                params.put("question6_dont_know", question6_dont_know_string);//

                params.put("appointment_remainder_date", appointment_remainder_date.getText().toString().trim());//
                params.put("appointment_remainder_time", appointment_remainder_time.getText().toString().trim());//

                params.put("time_taken",getArguments().getString("duration"));//
                params.put("distance",getArguments().getString("distance"));///
               // params.put("job_id",getArguments().getString("job_id"));//

             //   params.put("new_forms_id", new_form_id);//
                params.put("user_lat",user_lat);//
                params.put("user_lng",user_lng);//

                Log.d(TAG,params.toString());
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData()
            {
                Map<String, DataPart> params = new HashMap<>();
                long imageName = System.currentTimeMillis();
                params.put("live_photo", new DataPart(imageName + ".png",mData1));
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(volleyMultipartRequest);
    }

    public void hitNotSubmitApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        VolleyMultipartRequest volleyMultipartRequest = new  VolleyMultipartRequest(Request.Method.POST,NOTSUBMITAPI,new Response.Listener<NetworkResponse>()
        {
            @Override
            public void onResponse(NetworkResponse response)
            {
                try
                {
                    JSONObject object = new JSONObject(new String(response.data));
                    String status = object.getString("status");
                    String message = object.getString("message");
                    Log.d(TAG, "not_submit_response" + response.data.toString());
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    if (status.equals("true"))
                    {
                        Fragment fragment = new UpComingJobFragment();
                        FragmentTransaction ft = ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, fragment, "home").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                        ft.commit();
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String,String> params = new HashMap<>();
                params.put("user_id", user_id);      //
                params.put("time_taken",getArguments().getString("duration"));//
                params.put("distance",getArguments().getString("distance"));///
                params.put("job_id",getArguments().getString("job_id"));//
                params.put("user_lat",user_lat);//
                params.put("user_lng",user_lng);//

                Log.d(TAG,params.toString());
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(volleyMultipartRequest);
    }

    private void showPictureDialog()
    {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED)
        {
            return;
        }
        switch (clickImage)
        {
            case 1:
                if (requestCode == GALLERY)
                {
                    if (data != null)
                    {
                        Uri selectedImageUri = data.getData( );
                        ByteArrayOutputStream bytes = null;
                        try
                        {
                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                            bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                            mData1 = bytes.toByteArray();

                            String picturePath = getPath(getActivity(), selectedImageUri);
                            String[] bits = picturePath.split("/");
                            lastOne = bits[bits.length-1];
                            Log.d(TAG,lastOne);
                            filename.setText(lastOne);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (requestCode == CAMERA)
                {
                    //Uri selectedImageUri = data.getData();
                    try
                    {
                        bitmap = (Bitmap) data.getExtras().get("data");
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                        filename.setText("image1.jpg");
                        mData1 = bytes.toByteArray();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                break;


        }
    }
    public static String getPath(Context context, Uri uri)
    {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }


    private void checkRunTimePermission(View v)
    {
        checkPermissionCamera();
    }

    private void checkPermissionCamera()
    {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int storageCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED
                &&storageCheck == PackageManager.PERMISSION_GRANTED
                &&readStorage == PackageManager.PERMISSION_GRANTED)
        {
            clickImage=1;
            showPictureDialog();
        }
        else {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA}, PERM_CAMERA_REQUEST);
        }
    }


    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS)
        {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(getActivity(), "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(getActivity(), "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            gpsTracker=new GPSTracker(getActivity());
            user_lat=String.valueOf(gpsTracker.getLatitude());
            user_lng=String.valueOf(gpsTracker.getLongitude());

            if(question1_carwash.isChecked())
            {
                question1_carwash_string="1";
            }
            else {
                question1_carwash_string="0";
            }

            if(question1_loans.isChecked())
            {
                question1_loans_string="1";
            }
            else
            {
                question1_loans_string="0";
            }

            if(question1_savings_money.isChecked())
            {
                question1_savings_money_string="1";
            }
            else
            {
                question1_savings_money_string="0";
            }
            if(question1_other.isChecked())
            {
                question1_other_string="1";
            }
            else
            {
                question1_other_string="0";
            }

            if(question2_fear_talking_topic.isChecked())
            {
                question2_fear_talking_topic_string="1";
            }
            else
            {
                question2_fear_talking_topic_string="0";
            }

            if(question2_leave_later.isChecked())
            {
                question2_leave_later_string="1";
            }
            else
            {
                question2_leave_later_string="0";
            }

            if(question2_think_too_expensive.isChecked())
            {
                question2_think_too_expensive_string="1";
            }
            else {
                question2_think_too_expensive_string="0";
            }

            if(question2_do_not_think_you_qualify.isChecked())
            {
                question2_do_not_think_you_qualify_string="1";
            }
            else
            {
                question2_do_not_think_you_qualify_string="0";
            }

            if(question3_yes.isChecked())
            {
                question3_yes_string="1";
            }else
            {
                question3_yes_string="0";
            }

            if(question3_no.isChecked())
            {
                question3_no_string="1";
            }
            else {
                question3_no_string="0";
            }

            if(question4_cremation.isChecked())
            {
                question4_cremation_string="1";
            }
            else {
                question4_cremation_string="0";
            }

            if(question4_cremation.isChecked())
            {
                question4_cremation_string="1";
            }
            else {
                question4_cremation_string="0";
            }

            if(question4_burial.isChecked())
            {
                question4_burial_string="1";
            }
            else {
                question4_burial_string="0";
            }
            if(question5_spouse.isChecked())
            {
                question5_spouse_string="1";
            }
            else {
                question5_spouse_string="1";
            }
            if(question5_child.isChecked())
            {
                question5_child_string="1";
            }
            else
            {
                question5_child_string="0";
            }


            if(question5_family.isChecked())
            {
                question5_family_string="1";
            }
            else
            {
                question5_family_string="0";
            }


            if(question5_friend.isChecked())
            {
                question5_friend_string="1";
            }
            else
            {
                question5_friend_string="0";
            }


            if(question5_other.isChecked())
            {
                question5_other_string="1";
            }
            else
            {
                question5_other_string="0";
            }


            if(question5_dont_know.isChecked())
            {
                question5_dont_know_string="1";
            }
            else
            {
                question5_dont_know_string="0";
            }


            if(question6_car_wash.isChecked())
            {
                question6_car_wash_string="1";
            }
            else
            {
                question6_car_wash_string="0";
            }

            if(question6_loans.isChecked())
            {
                question6_loans_string="1";
            }
            else {
                question6_loans_string="0";
            }


            if(question6_savings.isChecked())
            {
                question6_savings_string="1";
            }
            else
            {
                question6_savings_string="0";
            }


            if(question6_other.isChecked())
            {
                question6_other_string="1";
            }
            else
            {
                question6_other_string="0";
            }
            if(question6_dont_know.isChecked())
            {
                question6_dont_know_string="1";
            }
            else
            {
                question6_dont_know_string="0";
            }
            if(language_english.isChecked())
            {
                language_english_String="1";
            }
            else
            {
                language_english_String="0";
            }
            if(langauge_spanish.isChecked())
            {
                langauge_spanish_string="1";
            }
            else
            {
                langauge_spanish_string="0";
            }
            if(validation())
            {
                String file_name=filename.getText().toString();
                String  str2=file_name.substring(file_name.lastIndexOf("."));
                if(str2.equals(".gif"))
                {
                    Toast.makeText(getActivity(), "Invalid File Format", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hitAddNewFormApi();
                }
            }
        }
    }

/*public void UserLatLng()
{
    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    String locationProvider = LocationManager.NETWORK_PROVIDER;
    // I suppressed the missing-permission warning because this wouldn't be executed in my
    // case without location services being enabled
    @SuppressLint("MissingPermission") android.location.Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
    double userLat = lastKnownLocation.getLatitude();
    double userLong = lastKnownLocation.getLongitude();

    try
    {
        user_lat=String.valueOf(userLat);
        user_lng=String.valueOf(userLong);

        Log.d(TAG, String.valueOf(userLat));
        Log.d(TAG, String.valueOf(userLong));
    }
    catch (Exception e)
    { }
}

    public static boolean isTomorrow(Date d)
    {
        return DateUtils.isToday(d.getTime() - DateUtils.DAY_IN_MILLIS);
    }*/
}