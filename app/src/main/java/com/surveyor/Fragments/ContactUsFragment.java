package com.surveyor.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.CONTACTUS;


public class ContactUsFragment extends Fragment implements View.OnClickListener
{
    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    EditText title,subject;
    Button btn_submit;
    String user_id;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_contact_us, container, false);
        init();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        imgBack=view.findViewById(R.id.imgBack);
        btn_submit=view.findViewById(R.id.btn_submit);
        title=view.findViewById(R.id.title);
        subject=view.findViewById(R.id.subject);

        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        txtTitleOther.setText("CONTACT US");
        imgBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

        user_id = getArguments().getString("user_id");
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                       hitContactUsApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;

                case R.id.imgBack:
                backpressed();
                break;
        }
    }
    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        //replacing the fragment
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
          //  ft.addToBackStack("");
            ft.commit();
        }
    }
    private void loadFragment(Fragment fragment)
    {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    private boolean  validation()
    {
        if (TextUtils.isEmpty(title.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter Title", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(subject.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter Subject", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hitContactUsApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            params.put("msg",title.getText().toString().trim());
            params.put("subject",subject.getText().toString().trim());
            Log.d(TAG,"contact_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,CONTACTUS, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d(TAG,"contact_response" +response);

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        title.getText().clear();
                        subject.getText().clear();

                        backpressed();
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Somthing Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }
}