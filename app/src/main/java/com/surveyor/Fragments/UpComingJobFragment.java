package com.surveyor.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.Adapters.UpComingJobsAdapter;
import com.surveyor.Models.UpComingJobsModel;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.GPSTracker;
import com.surveyor.Utils.ProgressD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.GETJOB;
import static com.surveyor.Utils.URLs.NOTIFICATIONCOUNT;

public class UpComingJobFragment extends Fragment implements View.OnClickListener
{
    View view;
    RecyclerView recyclerView;
    UpComingJobsAdapter upcomingJobsadapter;
    List<UpComingJobsModel> upComingJobsModelList;
    private FragmentTransaction ft;
    AppCompatImageView main_bell;
    TextView txtTitleMain,upcoming_title;
    String user_id;
    String name,address,mobile_no,lat,lang,job_id,new_forms_id;
    String image;
    LinearLayout linear_items,linear_nodata;
    TextView notificationcount_text;
    Button btn_active,btn_inactive,btn_submitform;

    String user_status;
    String tracker_id;
    SharedPreferences login;
    GPSTracker gpsTracker;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    String user_lat,user_lng;
    String survey_form_status="";
    String notificationcount;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_upcoming_jobs, container, false);
        init();
        user_id = LoginPreferences.getActiveInstance(getActivity()).getUserId();
        tracker_id=LoginPreferences.getActiveInstance(getActivity()).getTrackId();
        user_status=LoginPreferences.getActiveInstance(getActivity()).getUserStatus();


        if(user_status.equals("") || user_status.equals("null"))                                       //first time manage user data
        {
            notificationcount = LoginPreferences.getActiveInstance(getActivity()).getnotificationcount();

            if(notificationcount.equals("0"))
            {
                notificationcount_text.setVisibility(View.GONE);
            }
            else if(notificationcount.equals(""))
            {
                notificationcount_text.setVisibility(View.GONE);
            }
            else
            {
                notificationcount_text.setVisibility(View.VISIBLE);
                notificationcount_text.setText(notificationcount);
            }
             btn_inactive.setEnabled(false);
             btn_inactive.setBackgroundResource(R.drawable.round_button_red_fed);
             btn_active.setEnabled(true);
             btn_active.setBackgroundResource(R.drawable.round_button);

             btn_submitform.setVisibility(View.GONE);
        }
        else
        {
            hitnotificationcountApi();
            if(user_status.equals("active"))
            {
                btn_active.setEnabled(false);
                btn_active.setBackgroundResource(R.drawable.round_button_red_fed);
                btn_submitform.setVisibility(View.VISIBLE);

                btn_inactive.setEnabled(true);
                btn_inactive.setBackgroundResource(R.drawable.round_button);
            }
            else
            {
                btn_inactive.setEnabled(false);
                btn_inactive.setBackgroundResource(R.drawable.round_button_red_fed);

                btn_active.setEnabled(true);
                btn_active.setBackgroundResource(R.drawable.round_button);
                btn_submitform.setVisibility(View.GONE);
            }
        }
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    private void init()
    {
        upcoming_title=view.findViewById(R.id.upcoming_title);
        linear_items=view.findViewById(R.id.linear_items);
        linear_nodata=view.findViewById(R.id.linear_nodata);
        txtTitleMain=view.findViewById(R.id.txtTitleMain);
        txtTitleMain.setText("HOME");
        main_bell=view.findViewById(R.id.main_bell);
        main_bell.setOnClickListener(this);
        notificationcount_text=view.findViewById(R.id.notificationcount_text);
        btn_active=view.findViewById(R.id.btn_active);
        btn_inactive=view.findViewById(R.id.btn_inactive);
        recyclerView =view.findViewById(R.id.recycle_item_list);
        btn_submitform=view.findViewById(R.id.btn_submitform);
        upComingJobsModelList = new ArrayList<UpComingJobsModel>();

        btn_active.setOnClickListener(this);
        btn_inactive.setOnClickListener(this);
        btn_submitform.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.main_bell:
                NotificationFragment ldff = new NotificationFragment();
                Bundle arg = new Bundle();
                arg.putString("user_id", user_id);
                ldff.setArguments(arg);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, ldff).addToBackStack(null).commit();
                break;

            case R.id.btn_active:
                if(CommonMethod.isOnline(getActivity()))
                {
                    checkPermission("active");
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;

            case R.id.btn_inactive:
                if(CommonMethod.isOnline(getActivity()))
                {
                    checkPermission("inactive");
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;


            case R.id.btn_submitform:
                if(CommonMethod.isOnline(getActivity()))
                {
                    AddNewFormFragment ldf = new AddNewFormFragment();
                    Bundle arg1 = new Bundle();
                  //  arg1.putString("job_id", job_id);
                    ldf.setArguments(arg1);
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,ldf).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                    survey_form_status="1";
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;
        }
    }
    private void checkPermission(String status)
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS(status);
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(getActivity(), "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }

    private void checkGPS(String user_status)
    {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(getActivity(), "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            gpsTracker=new GPSTracker(getActivity());
            user_lat=String.valueOf(gpsTracker.getLatitude());
            user_lng=String.valueOf(gpsTracker.getLongitude());
            hitgetjobApi(user_status);
        }
    }

    public void hitgetjobApi(String user_current_status)
    {
    int MY_SOCKET_TIMEOUT_MS=6000;
    final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
    JSONObject params = new JSONObject();
    try
    {
        params.put("user_id",LoginPreferences.getActiveInstance(getActivity()).getUserId());
        params.put("user_status",user_current_status);
        params.put("tracking_id",LoginPreferences.getActiveInstance(getActivity()).getTrackId());
        params.put("user_lat",user_lat);
        params.put("user_lng",user_lng);
        Log.d(TAG,"getjobinactive_params"+ params.toString());
    }
    catch
    (Exception e) { }
    JsonObjectRequest jsonObjReq;
    jsonObjReq = new JsonObjectRequest(Request.Method.POST,GETJOB, params,new Response.Listener<JSONObject>()
    {
        @Override
        public void onResponse(JSONObject response)
        {
            try
            {
                progressDialog.dismiss();
                String status = response.getString("status");
                notificationcount = response.getString("notifyCount");
                user_status=response.getString("user_status");
                tracker_id=response.getString("tracker_id");
                LoginPreferences.getActiveInstance(getActivity()).setNotificationcount(notificationcount);
                LoginPreferences.getActiveInstance(getActivity()).setUserStatus(user_status);
                LoginPreferences.getActiveInstance(getActivity()).setTrackId(tracker_id);

                if(notificationcount.equals("0"))
                {
                    notificationcount_text.setVisibility(View.GONE);
                }
                else if(notificationcount.equals(""))
                {
                    notificationcount_text.setVisibility(View.GONE);
                }
                else
                {
                    notificationcount_text.setVisibility(View.VISIBLE);
                    notificationcount_text.setText(notificationcount);
                }
                if(!status.equals(null))
                {
                    if (status.equals("true"))
                    {
                        if(user_status.equals("active"))
                        {
                            btn_inactive.setEnabled(true);
                            btn_inactive.setBackgroundResource(R.drawable.round_button);
                            btn_submitform.setVisibility(View.VISIBLE);

                            btn_active.setEnabled(false);
                            btn_active.setBackgroundResource(R.drawable.round_button_red_fed);
                        }
                        else
                        {
                            try
                            {
                                btn_active.setEnabled(true);
                                btn_active.setBackgroundResource(R.drawable.round_button);
                                btn_submitform.setVisibility(View.GONE);

                                btn_inactive.setEnabled(false);
                                btn_inactive.setBackgroundResource(R.drawable.round_button_red_fed );
                                upComingJobsModelList.clear();
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                else
                { }
            } catch (JSONException e)
            {
                e.printStackTrace();
                progressDialog.dismiss();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            Log.d(TAG, error.getMessage() + "error11");
            Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
            //  progressbar.setVisibility(GONE);
            progressDialog.dismiss();
            error.printStackTrace();
        }
    })
    {};
    jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
            MY_SOCKET_TIMEOUT_MS,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
}


    public void hitnotificationcountApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id",LoginPreferences.getActiveInstance(getActivity()).getUserId());
            Log.d(TAG,"getjobinactive_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,NOTIFICATIONCOUNT, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    progressDialog.dismiss();
                    String status = response.getString("status");
                    notificationcount = response.getString("result");
                    LoginPreferences.getActiveInstance(getActivity()).setNotificationcount(notificationcount);
                    if(notificationcount.equals("0"))
                    {
                        notificationcount_text.setVisibility(View.GONE);
                    }
                    else if(notificationcount.equals(""))
                    {
                        notificationcount_text.setVisibility(View.GONE);
                    }
                    else
                    {
                        notificationcount_text.setVisibility(View.VISIBLE);
                        notificationcount_text.setText(notificationcount);
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }
}