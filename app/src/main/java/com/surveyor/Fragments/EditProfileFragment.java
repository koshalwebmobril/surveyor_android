package com.surveyor.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;
import com.surveyor.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;
import static com.surveyor.Utils.URLs.UPDATEPROFILE;
public class EditProfileFragment extends Fragment implements View.OnClickListener,ActivityCompat.OnRequestPermissionsResultCallback
{
    View view;
    AppCompatImageView imgBack;
    TextView txtTitleOther;
    EditText name,phone_no;
    Button btn_submit;
    String username,mobileno,profile_image,user_id;
    ImageView profile_imageview,click_image;
    byte[] mData1;
    private int GALLERY = 1, CAMERA = 2;
    private int clickImage;
    Bitmap bitmap = null;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private static final int PERM_CAMERA_REQUEST = 123;
    int len;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_edit_profile, container, false);
        init();

        phone_no.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (len > phone_no.getText().length())
                {
                    len--;
                    return;
                }
                len = phone_no.getText().length();

                if (len == 4 || len== 8) {
                    String number = phone_no.getText().toString();
                    String dash = number.charAt(number.length() - 1) == '-' ? "" : "-";
                    number = number.substring(0, (len - 1)) + dash + number.substring((len - 1), number.length());
                    phone_no.setText(number);
                    phone_no.setSelection(number.length());
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        imgBack=view.findViewById(R.id.imgBack);
        txtTitleOther=view.findViewById(R.id.txtTitleOther);
        name=view.findViewById(R.id.username);
        phone_no=view.findViewById(R.id.phone_no);
        btn_submit=view.findViewById(R.id.btn_submit);
        profile_imageview=view.findViewById(R.id.profile_imageview);
        click_image=view.findViewById(R.id.click_image);

        txtTitleOther.setText("EDIT PROFILE");
        imgBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        click_image.setOnClickListener(this);

        username = getArguments().getString("username");
        mobileno = getArguments().getString("phoneno");
        user_id = getArguments().getString("user_id");
        profile_image = getArguments().getString("profile_image");

        name.setText(username);
        name.setEnabled(false);
        phone_no.setText(mobileno);

        if(profile_image.equals("null")) {
            profile_imageview.setImageResource(R.drawable.user_profile_icon);
        }
        else
        {
            Picasso.get().load(IMAGE_BASE_URL+profile_image).placeholder(getResources().getDrawable(R.drawable.user_profile_icon)).error(getResources().getDrawable(R.drawable.user_profile_icon)).into(profile_imageview);
        }
        try
        {
            bitmap=((BitmapDrawable)profile_imageview.getDrawable()).getBitmap();          //get avatar bitmap image when null imagew come
        }
        catch (Exception e)
        { }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if (CommonMethod.isOnline(getActivity()))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                       hitUpdateProfileApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                }
                break;

            case R.id.imgBack:
                backpressed();
                break;
            case R.id.click_image:
                checkRunTimePermission(v);
                break;
        }
    }

    public void backpressed()
    {
        Fragment fragment = new AccountFragment();
        if (fragment != null)
        {
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment,"account");
           // ft.addToBackStack("");
            ft.commit();
        }
    }

    @Nullable
    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    private boolean  validation()
    {
        if (TextUtils.isEmpty(name.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(phone_no.getText().toString().trim()))
        {
            Toast.makeText(getActivity(), "Please enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().length()<7 || phone_no.getText().toString().trim().length()>15 )
        {
            Toast.makeText(getActivity(), "Phone number must be 7 to 15 digit", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("0000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(phone_no.getText().toString().trim().equals("00000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("0000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("00000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("0000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("00000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(phone_no.getText().toString().trim().equals("000000000000000"))
        {
            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    private void showPictureDialog()
{
    AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
    pictureDialog.setTitle("Select Action");
    String[] pictureDialogItems = {
            "Select photo from gallery",
            "Capture photo from camera"};
    pictureDialog.setItems(pictureDialogItems,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    switch (which) {
                        case 0:
                            choosePhotoFromGallary();
                            break;
                        case 1:
                            takePhotoFromCamera();
                            break;
                    }
                }
            });
    pictureDialog.show();
}
    public void choosePhotoFromGallary()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public void hitUpdateProfileApi()
    {
        int MY_SOCKET_TIMEOUT_MS=6000;

        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        VolleyMultipartRequest volleyMultipartRequest = new  VolleyMultipartRequest(Request.Method.POST,UPDATEPROFILE,new Response.Listener<NetworkResponse>()
        {
            @Override
            public void onResponse(NetworkResponse response)
            {
                try
                {
                    JSONObject object = new JSONObject(new String(response.data));
                    String status = object.getString("status");
                    String message = object.getString("message");
                    Log.d(TAG, "register_response" + response.data);

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    if (status.equals("true"))
                    {
                        /*EditProfileFragment ldff = new EditProfileFragment();
                        getFragmentManager().beginTransaction().replace(R.id.fragment_container, ldff).addToBackStack(null).commit();*/
                        backpressed();
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something went wrong..Please try again later", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("user_id",LoginPreferences.getActiveInstance(getActivity()).getUserId());
                params.put("mobile", phone_no.getText().toString().trim());
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData()
            {
                Map<String, DataPart> params = new HashMap<>();
                long imageName = System.currentTimeMillis();
                params.put("profile_image", new DataPart(imageName + ".png",getFileDataFromDrawable(bitmap)));
                Log.d(TAG,params.toString());
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Mysingleton.getInstance(getActivity()).addTorequestque(volleyMultipartRequest);
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED)
        {
            return;
        }
        switch (clickImage)
        {
            case 1:
                if (requestCode == GALLERY)
                {
                    if (data != null)
                    {
                        Uri selectedImageUri = data.getData( );
                        ByteArrayOutputStream bytes = null;
                        try
                        {
                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                            bytes = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                            mData1 = bytes.toByteArray();
                           profile_imageview.setImageBitmap(bitmap);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (requestCode == CAMERA)
                {
                    try
                    {
                        bitmap = (Bitmap) data.getExtras().get("data");
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                        mData1 = bytes.toByteArray();
                        profile_imageview.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }



    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }


    public static String getPath(Context context, Uri uri)
    {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

    private void checkRunTimePermission(View v)
    {
        checkPermissionCamera();
    }

    private void checkPermissionCamera()
    {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int storageCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED
                &&storageCheck == PackageManager.PERMISSION_GRANTED
                &&readStorage == PackageManager.PERMISSION_GRANTED)
        {
            clickImage=1;
            showPictureDialog();
        }

        else {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA}, PERM_CAMERA_REQUEST);
        }
    }

}