package com.surveyor.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.surveyor.Activities.DrawPath;
import com.surveyor.Activities.Mysingleton;
import com.surveyor.R;
import com.surveyor.Sharedpreference.LoginPreferences;
import com.surveyor.Utils.CommonMethod;
import com.surveyor.Utils.ProgressD;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;

import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;
import static com.surveyor.Utils.URLs.NAVIGATIONAPI;

public class NavigationFragment extends Fragment implements View.OnClickListener,OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    View view;
    AppCompatImageView imgBack;
    Button btn_start;
    TextView txtTitleOther;
    private GoogleMap mMap;
    private double current_location_longitude;
    private double current_location_latitude;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String name, address, mobile_no, image,lat,lng,job_id,user_id;
    ImageView profile_imageview;
    TextView name_text, address_text, phone_text;
    ProgressBar progress_bar;
    SupportMapFragment mapFragment;

    private LatLng latLng_origin;
    private LatLng mDestination;

    ArrayList<LatLng> mMarkerPoints;
    double latitude_destination,longitude_destination;
    int orderStatus=0;
    Button btn_end;
    static String distance;
    static String duration;
    String path="";
    Marker mCurrLocationMarker,mDestinationLocationMarker;
    String survey_form_status="";
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_navigation, container, false);
        init();
        checkLocationPermission();
        checkGPS();
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mMarkerPoints = new ArrayList<>();
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        final SharedPreferences login = getActivity().getSharedPreferences("Login_Token", MODE_PRIVATE);
        name = login.getString("name", "");
        address = login.getString("address", "");
        mobile_no = login.getString("phone_no", "");
        image = login.getString("image", "");
        lat=login.getString("lat","");
        lng=login.getString("lng","");
        job_id=login.getString("job_id","");
        user_id=login.getString("user_id","");



         latitude_destination = Double.parseDouble(lat);
         longitude_destination = Double.parseDouble(lng);

        imgBack = view.findViewById(R.id.imgBack);
        btn_start = view.findViewById(R.id.btn_start);
        txtTitleOther = view.findViewById(R.id.txtTitleOther);
        profile_imageview = view.findViewById(R.id.profile_image);
        name_text = view.findViewById(R.id.name_text);
        phone_text = view.findViewById(R.id.phone_text);
        address_text = view.findViewById(R.id.address_text);
        progress_bar = view.findViewById(R.id.progress_bar);
        btn_end=view.findViewById(R.id.btn_end);


        btn_start.setVisibility(View.VISIBLE);
        txtTitleOther.setText("NAVIGATION");
        imgBack.setOnClickListener(this);
        btn_start.setOnClickListener(this);
        btn_end.setOnClickListener(this);

        if (image.equals("null"))
        {
            profile_imageview.setImageResource(R.drawable.profile);
        } else {
            Picasso.get()
                    .load(IMAGE_BASE_URL + image)
                    .into(profile_imageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progress_bar.setVisibility(GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            progress_bar.setVisibility(GONE);
                            profile_imageview.setImageResource(R.drawable.user_profile_icon);
                        }
                    });
        }
        name_text.setText(StringUtils.capitalize(name.toLowerCase().trim()));
        address_text.setText(address);
        phone_text.setText(mobile_no);

        mDestination = new LatLng(latitude_destination, longitude_destination);

        /*if(getArguments()!=null)
        {
            survey_form_status=getArguments().getString("survey_form_status");

            btn_start.setVisibility(GONE);
            btn_end.setVisibility(View.VISIBLE);
        }*/
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_start:
                if (CommonMethod.isOnline(getActivity()))
                {
                     DrawPath drawpath = new DrawPath(mMap,current_location_latitude, current_location_longitude,latitude_destination,longitude_destination,orderStatus);

                     btn_start.setVisibility(GONE);
                     btn_end.setVisibility(View.VISIBLE);
                    // survey_form_status="1";
                }
                else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), getActivity());
                    }
                break;

            case R.id.imgBack:
                backpressed();
                break;

            case R.id.btn_end:
                hitNavigationApi();
                //  getDistance(current_location_latitude,current_location_longitude,);
                break;
        }
    }
    public void backpressed()
    {
        Fragment fragment = new UpComingJobFragment();
        if (fragment != null) {
            FragmentTransaction ft = ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment, "home").setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
    }
    @Override
    public void onLocationChanged(Location location)
    {
        try
        {
            mLastLocation = location;
            current_location_latitude = location.getLatitude();
            current_location_longitude = location.getLongitude();
            latLng_origin = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng_origin, 11));
            Log.d(TAG, "latlng" +latLng_origin);

            if(mCurrLocationMarker != null)
            {
                mCurrLocationMarker.remove();
            }
            else
                {
                MarkerOptions markerOption1 = new MarkerOptions();
                markerOption1.position(latLng_origin);
                markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                mCurrLocationMarker = mMap.addMarker(markerOption1);
               }
            if(mDestinationLocationMarker !=null)
            {
                mDestinationLocationMarker.remove();
            }
           else
            {
                MarkerOptions markerOption2 = new MarkerOptions();
                markerOption2.position(mDestination);
                markerOption2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mCurrLocationMarker = mMap.addMarker(markerOption2);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    { }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else
                {
                  checkLocationPermission();
               }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private void checkGPS() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getActivity(), "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        } else
            {}
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) getActivity());
            } catch (Exception e) {
                // Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mapFragment.onResume();
       /* if(survey_form_status.equals("1"))
        {
            btn_end.setVisibility(View.VISIBLE);
            btn_start.setVisibility(GONE);
        }
        else
        {
            btn_start.setVisibility(View.VISIBLE);
            btn_end.setVisibility(GONE);
        }*/
    }

    public static class DataParser
    {
        public List<List<HashMap<String, String>>> parse(JSONObject jObject)
        {
            List<List<HashMap<String, String>>> routes = new ArrayList<>();
            JSONArray jRoutes;
            JSONArray jLegs;
            JSONArray jSteps;
            JSONObject jDistance,jDuration;
            try
            {
                jRoutes = jObject.getJSONArray("routes");
                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++)
                {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    jDistance = ((JSONObject) jLegs.get(i)).getJSONObject("distance");
                    jDuration = ((JSONObject) jLegs.get(i)).getJSONObject("duration");
                    distance=jDistance.getString("text");
                    duration=jDuration.getString("text");
                    List path = new ArrayList<>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");


                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("lat", Double.toString((list.get(l)).latitude));
                                hm.put("lng", Double.toString((list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return routes;
        }

        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }
    }

    public void hitNavigationApi()
    {
        final ProgressD progressDialog = ProgressD.showLoading(getActivity(),"please wait");
        JSONObject params = new JSONObject();
        try
        {
            params.put("user_id", LoginPreferences.getActiveInstance(getActivity()).getUserId());
            params.put("job_id",job_id);
            params.put("distance",distance);
            params.put("time_taken",duration);
            Log.d(TAG,"navigation_params"+ params.toString());
        }
        catch
        (Exception e) { }
        JsonObjectRequest jsonObjReq;
        jsonObjReq = new JsonObjectRequest(Request.Method.POST,NAVIGATIONAPI, params,new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d(TAG,"navigation_response" +response);
                    if (status.equals("true"))
                    {
                        AddNewFormFragment ldff = new AddNewFormFragment();
                        Bundle arg1 = new Bundle();
                        arg1.putString("distance", distance);
                        arg1.putString("duration", duration);
                        arg1.putString("job_id", job_id);
                        ldff.setArguments(arg1);
                        getFragmentManager().beginTransaction().replace(R.id.fragment_container,ldff).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                        survey_form_status="1";
                        progressDialog.dismiss();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        btn_start.setVisibility(View.VISIBLE);
                        btn_end.setVisibility(GONE);
                        progressDialog.dismiss();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.getMessage() + "error11");
                Toast.makeText(getActivity(), "Something Went Wrong..Try Again", Toast.LENGTH_SHORT).show();
                //  progressbar.setVisibility(GONE);
                progressDialog.dismiss();
                error.printStackTrace();
            }
        })
        {};
        Mysingleton.getInstance(getActivity()).addTorequestque(jsonObjReq);
    }




    public String getDistance(final double lat1, final double lon1, final double lat2, final double lon2)
    {
        final String[] parsedDistance = new String[1];
        final String[] response = new String[1];
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric&mode=driving");
                    final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response[0] = org.apache.commons.io.IOUtils.toString(in, "UTF-8");

                    JSONObject jsonObject = new JSONObject(response[0]);
                    JSONArray array = jsonObject.getJSONArray("routes");
                    JSONObject routes = array.getJSONObject(0);
                    JSONArray legs = routes.getJSONArray("legs");
                    JSONObject steps = legs.getJSONObject(0);
                    JSONObject distance = steps.getJSONObject("distance");
                    parsedDistance[0] =distance.getString("text");

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return parsedDistance[0];
    }
}