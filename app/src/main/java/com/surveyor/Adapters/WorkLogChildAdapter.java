package com.surveyor.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.surveyor.Fragments.WorkLogDetailsFragment;
import com.surveyor.Interface.OnFragmentClick;
import com.surveyor.Models.ResponseModel;
import com.surveyor.R;

import java.util.List;

import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;

public class WorkLogChildAdapter extends RecyclerView.Adapter<WorkLogChildAdapter.MyViewHolder>
{
    List<ResponseModel.Result.Data> workloglist;
    Context context;
    private OnFragmentClick mCallback;
    ImageView user_image;

    public WorkLogChildAdapter(Context context, List<ResponseModel.Result.Data> workloglist)
    {
        this.context = context;
        this.workloglist = workloglist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_log_child_itemlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final ResponseModel.Result.Data worklogItem = workloglist.get(position);
        final String image = worklogItem.getLive_photo();
        final String name = worklogItem.getName();
        final String address = worklogItem.getAddress();
        final String phone_no = worklogItem.getMobile();
        final String description = worklogItem.getDesc();
        final String distance = worklogItem.getDistance();
        final String time_taken = worklogItem.getTime_taken();
        final String submitted_date = worklogItem.getCreated_at();

        Picasso.get().load(IMAGE_BASE_URL+image).placeholder(context.getResources().getDrawable(R.drawable.user_profile_icon)).error(context.getResources().getDrawable(R.drawable.user_profile_icon)).into(user_image);
        holder.name.setText(name);
        holder.address.setText(address);
        holder.phone_no.setText(phone_no);


        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                WorkLogDetailsFragment worklogdetail = new WorkLogDetailsFragment();
                SharedPreferences login = context.getSharedPreferences("Login_Token", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = login.edit();
                editor.putString("image", image);
                editor.putString("name", name);
                editor.putString("submitted_date",submitted_date);
                editor.putString("address", address);
                editor.putString("phone_no", phone_no);
                editor.putString("time_taken", time_taken);
                editor.putString("distance", distance);
                editor.putString("description", description);
                editor.commit();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,  worklogdetail).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return workloglist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,address,phone_no,date_text;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.title);
            address = itemView.findViewById(R.id.title_text);
            phone_no = itemView.findViewById(R.id.phone_no);
            user_image = itemView.findViewById(R.id.user_image);
        }
    }
}