package com.surveyor.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.surveyor.Models.ResponseModel;
import com.surveyor.R;

import java.util.ArrayList;
import java.util.List;

public class WorkLogAdapter extends RecyclerView.Adapter<WorkLogAdapter.MyViewHolder>
{
  //  ArrayList<WorkLogModel> workloglist;
    //ArrayList<WorkLogModel> worklogchildlist=new ArrayList<>();

    List<ResponseModel.Result> list;
    Context context;
    ImageView user_image;
    RecyclerView recycle_child;
    WorkLogChildAdapter workLogChildAdapter;

    public WorkLogAdapter(Context context, List<ResponseModel.Result> workloglist)
    {
        this.context = context;
        this.list = workloglist;
     // this.worklogchildlist =worklogchildlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_log_itemlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         ResponseModel.Result worklogItem = list.get(position);

         holder.date_text.setText(worklogItem.getDate());

        recycle_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        workLogChildAdapter = new WorkLogChildAdapter(context, (ArrayList) worklogItem.getData());
        recycle_child.setAdapter(workLogChildAdapter);

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView date_text;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            date_text=itemView.findViewById(R.id.date);
            recycle_child =itemView.findViewById(R.id.recycle_child);
        }
    }
}