package com.surveyor.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.surveyor.Fragments.NavigationFragment;
import com.surveyor.Interface.OnFragmentClick;
import com.surveyor.Models.UpComingJobsModel;
import com.surveyor.R;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;

public class UpComingJobsAdapter extends RecyclerView.Adapter<UpComingJobsAdapter.MyViewHolder>
{
    ArrayList<UpComingJobsModel> upcomingjobslist;
    Context context;
    private OnFragmentClick mCallback;
    ImageView profile_image;

    public UpComingJobsAdapter(Context context, ArrayList upcomingjobslist)
    {
        this.context = context;
        this.upcomingjobslist = upcomingjobslist;
      //  this.mCallback= mCallback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.jobs_itemlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final UpComingJobsModel jobsItem = upcomingjobslist.get(position);
        final String image = jobsItem.getImage();
        final String name = jobsItem.getName();
        final String address = jobsItem.getAddress();
        final String phone_no = jobsItem.getPhoneno();
        final String lat = jobsItem.getLat();
        final String lng = jobsItem.getLang();
        final String job_id = jobsItem.getJobId();
        final String new_form_id = jobsItem.getNewFormId();

        if(image.equals("null"))
        {
            profile_image.setImageResource(R.drawable.profile);
        }
        else
        {
            Picasso.get().load(IMAGE_BASE_URL+image).into(profile_image);
        }

        holder.name.setText(StringUtils.capitalize(name.toLowerCase().trim()));
        holder.address.setText(address);
        holder.phone_no.setText(phone_no);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                NavigationFragment navigation = new NavigationFragment();
                SharedPreferences login = context.getSharedPreferences("Login_Token", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = login.edit();
                editor.putString("name", name);
                editor.putString("address", address);
                editor.putString("phone_no", phone_no);
                editor.putString("image",image);
                editor.putString("lat",lat);
                editor.putString("lng",lng);
                editor.putString("job_id",job_id);
                editor.putString("new_form_id",new_form_id);
                editor.commit();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, navigation).addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return upcomingjobslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,address,phone_no;


        public MyViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            phone_no = itemView.findViewById(R.id.phone_no);
            profile_image = itemView.findViewById(R.id.user_image);
        }
    }
}