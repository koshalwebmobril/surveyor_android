package com.surveyor.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.surveyor.R;
import com.surveyor.Fragments.HistoryDetailFragment;
import com.surveyor.Models.HistoryModel;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.surveyor.Utils.URLs.IMAGE_BASE_URL;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder>
{
    ArrayList<HistoryModel> historylist;
    Context context;
    LinearLayout linear_nodata;
    ImageView profile_image;

    public HistoryAdapter(Context context, ArrayList historylist)
    {
        this.context = context;
        this.historylist = historylist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final HistoryModel historyItem = historylist.get(position);
        final String image = historyItem.getImage();
        final String name = historyItem.getName();
        final String address = historyItem.getAddress();
        final String phone_no = historyItem.getPhoneno();
        final String time_taken = historyItem.getTimeTaken();
        final String distance = historyItem.getDistance();
        Log.d(TAG,image);
        Glide.with(context).load(IMAGE_BASE_URL+image).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(profile_image);

        holder.name.setText(StringUtils.capitalize(name.toLowerCase().trim()));
        holder.address.setText(address);
        holder.phone_no.setText(phone_no);
        holder.time.setText(time_taken);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                HistoryDetailFragment historydetails = new HistoryDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("name", name);
                bundle.putString("address", address);
                bundle.putString("phone_no", phone_no);
                bundle.putString("image", historylist.get(position).getImage());
                bundle.putString("distance", distance);
                bundle.putString("time", time_taken);
                historydetails.setArguments(bundle);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, historydetails).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return historylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,address,phone_no,time;
        LinearLayout linear_nodata;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            phone_no = itemView.findViewById(R.id.phone_no);
            profile_image = itemView.findViewById(R.id.user_image);
            time=itemView.findViewById(R.id.time);
            linear_nodata=itemView.findViewById(R.id.linear_nodata);
        }
    }
}