package com.surveyor.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.surveyor.R;
import com.surveyor.Models.NotificationModel;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    ArrayList<NotificationModel> notificationmodellist;
    Context context;
    String time;

    public NotificationAdapter(Context context, ArrayList notificationmodellist)
    {
        this.context = context;
        this.notificationmodellist = notificationmodellist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final NotificationModel notificationItem = notificationmodellist.get(position);
        final String title = notificationItem.getTitle();
        final String title_text = notificationItem.getTitleText();
        final String timing = notificationItem.getTiming();

        holder.profile_image.setImageResource(R.drawable.bell_color_icon);
        holder.title.setText(title);
        holder.title_text.setText(title_text);
       /* try
        {
             String[] splited = timing.split("\\s+");
             time = splited[1];
        }
        catch (Exception e)
        { }

        try
        {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            String s= new SimpleDateFormat("K:mm a").format(dateObj);
            holder.timing.setText(s);
        }
        catch (final ParseException e) {
            e.printStackTrace();
        }*/
        StringTokenizer tk = new StringTokenizer(timing);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        Date dt;
        try
        {
            dt = sdf.parse(time);
            holder.timing.setText(sdfs.format(dt));
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount()
    {
        return notificationmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,title_text,timing;
        ImageView profile_image;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            title_text = itemView.findViewById(R.id.title_text);
            profile_image = itemView.findViewById(R.id.user_image);
            timing=itemView.findViewById(R.id.timing);
        }
    }
}